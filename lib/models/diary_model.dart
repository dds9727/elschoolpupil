class Lesson {
  DateTime start, end;
  String title, homework, mark, room, classwork, note, teacherName;
  int number;

  Lesson({
    this.start,
    this.end,
    this.title,
    this.mark,
    this.classwork,
    this.homework,
    this.note,
    this.number,
    this.room,
    this.teacherName,
  });

  factory Lesson.fromJson(Map<String, dynamic> json) => Lesson(
    start: DateTime.parse(json['start']),
    end: DateTime.parse(json['end']),
    title: json['title'],
    mark: json['mark'],
    classwork: json['classwork'],
    homework: json['homework'],
    note: json['note'],
    number: json['number'],
    room: json['room'],
    teacherName: json['teacherName'],
  );
}

class DaySchedule {
  DateTime date;
  bool isLoading;
  List<Lesson> lessons;

  DaySchedule({this.date, this.lessons, this.isLoading = false});
}
