class SchoolInfo {
  int id;
  String name, principleName, districtName, phone, email, address;

  SchoolInfo({this.id, this.name, this.phone, this.address, this.districtName, this.email, this.principleName});

  factory SchoolInfo.fromJson(Map<String, dynamic> json) => SchoolInfo(
    id: json['id'],
    name: json['name'],
    principleName: json['principleName'],
    districtName: json['districtName'],
    phone: json['phone'],
    email: json['email'],
    address: json['address'],
  );
}
