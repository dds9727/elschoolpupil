import 'package:flutter/material.dart';

const int _intPrimaryColor = 0xFF045772;
const MaterialColor primaryColor = MaterialColor(
  _intPrimaryColor,
  <int, Color>{
    50: Color(0xFF045772),
    100: Color(0xFF045772),
    200: Color(0xFF045772),
    300: Color(0xFF045772),
    400: Color(0xFF045772),
    500: Color(0xFF045772),
    600: Color(0xFF045772),
    700: Color(0xFF045772),
    800: Color(0xFF045772),
    900: Color(0xFF045772),
  },
);

const Color cardColor = Color(0xFFE9F0F5);

class NoGlowBehaviour extends ScrollBehavior {
  @override
  Widget buildViewportChrome(BuildContext context, Widget child, AxisDirection axisDirection) {
    return child;
  }

  @override
  ScrollPhysics getScrollPhysics(BuildContext context) => ClampingScrollPhysics();
}