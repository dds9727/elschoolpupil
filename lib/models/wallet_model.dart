import 'dart:convert';
import 'package:http/http.dart' as http;

class FoodPageModel {
  int userId;
  List<BalanceModel> balances;
  List<TransactionModel> transactions;

  FoodPageModel({this.userId, this.balances, this.transactions});

  factory FoodPageModel.fromJson(Map<String, dynamic> json) => FoodPageModel(
        userId: json['userId'],
        balances: (json['balances'] as List).map((i) => BalanceModel.fromJson(i)).toList(),
        transactions: (json['transactions'] as List).map((i) => TransactionModel.fromJson(i)).toList(),
      );

  static Future<FoodPageModel> getAccountData(childId) async {
    var response;
    if (childId == 1488) {
      response = await http.get(Uri.https('api.mocki.io', '/v1/ef4d7201'));
    } else if (childId == 1489) {
      response = await http.get(Uri.https('api.mocki.io', '/v1/a9b436d8'));
    } else if (childId == 1492) {
      response = await http.get(Uri.https('api.mocki.io', '/v1/4a65e74f'));
    }  else {
      response = await http.get(Uri.https('api.mocki.io', '/v1/dcb21866'));
    }
    return FoodPageModel.fromJson(json.decode(response.body));
  }
}

class BalanceModel {
  String name;
  double balance;

  BalanceModel({this.name, this.balance});

  factory BalanceModel.fromJson(Map<String, dynamic> json) => BalanceModel(
        name: json['name'],
        balance: json['balance'].toDouble(),
      );
}

class TransactionModel {
  final bool isIncome;
  final String title, keyword;
  final double amount, fullCost, compensation;
  final DateTime dateTime;

  TransactionModel({this.title, this.keyword, this.dateTime, this.isIncome, this.amount, this.compensation = 0, this.fullCost = 0});

  factory TransactionModel.fromJson(Map<String, dynamic> json) => TransactionModel(
        title: json['title'],
        keyword: json['keyword'],
        dateTime: DateTime.parse(json['dateTime']),
        isIncome: json['isIncome'],
        amount: json['amount'] != null ? json['amount'].toDouble() : 0,
        compensation: json['compensation'] != null ? json['compensation'].toDouble() : 0,
        fullCost: json['fullCost'] != null ? json['fullCost'].toDouble() : 0,
      );
}

class BalanceSum {
  String keyword;
  double amount;
  bool isSelected;

  BalanceSum({this.keyword, this.amount, this.isSelected});
}