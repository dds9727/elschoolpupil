class MessengerContact {
  final String name, pictureUrl;
  final int chatId, unreadCounter;
  final bool isConference;
  final List<ChatMessage> messages;

  MessengerContact({
    this.chatId,
    this.name,
    this.pictureUrl,
    this.messages,
    this.isConference = false,
    this.unreadCounter = 0,
  });

  factory MessengerContact.fromJson(Map<String, dynamic> json) => MessengerContact(
    name: json['name'],
    pictureUrl: json['pictureUrl'],
    chatId: json['chatId'],
    unreadCounter: json['unreadCounter'],
    isConference: json['isConference'] == true,
    messages: (json['messages'] as List).map((i) => ChatMessage.fromJson(i)).toList(),
  );
}

class ChatMessage {
  final String text, messageStatus, senderName;
  final DateTime dateTime;
  final int senderId;

  ChatMessage({this.text, this.messageStatus, this.dateTime, this.senderId, this.senderName});

  factory ChatMessage.fromJson(Map<String, dynamic> json) => ChatMessage(
    text: json['text'],
    messageStatus: json['messageStatus'],
    senderName: json['senderName'],
    dateTime: DateTime.parse(json['dateTime']),
    senderId: json['senderId'],
  );
}
