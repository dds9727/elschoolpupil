class SubjectGrade {
  String title;
  List<PeriodGrades> subjectGrades;

  SubjectGrade({this.title, this.subjectGrades});

  factory SubjectGrade.fromJson(Map<String, dynamic> json) => SubjectGrade(
    title: json['title'],
    subjectGrades: (json['subjectGrades'] as List).map((i) => PeriodGrades.fromJson(i)).toList(),
  );
}

class PeriodGrades {
  String periodName;
  List<int> grades;

  PeriodGrades({this.periodName, this.grades});

  factory PeriodGrades.fromJson(Map<String, dynamic> json) => PeriodGrades(
    periodName: json['periodName'],
    grades: json['grades'] != null ? List.from(json['grades']) : [],
  );
}

class SubjectGradesAndAbsences {
  String title;
  List<PeriodGradesAndAbsences> subjectGrades;

  SubjectGradesAndAbsences({this.title, this.subjectGrades});

  factory SubjectGradesAndAbsences.fromJson(Map<String, dynamic> json) => SubjectGradesAndAbsences(
    title: json['title'],
    subjectGrades: (json['subjectGrades'] as List).map((i) => PeriodGradesAndAbsences.fromJson(i)).toList(),
  );
}

class PeriodGradesAndAbsences {
  String periodName;
  List<String> grades;

  PeriodGradesAndAbsences({this.periodName, this.grades});

  factory PeriodGradesAndAbsences.fromJson(Map<String, dynamic> json) => PeriodGradesAndAbsences(
    periodName: json['periodName'],
    grades: json['grades'] != null ? List.from(json['grades']) : [],
  );
}

class SubjectResults {
  String title;
  List<PeriodResult> resultGrades;

  SubjectResults({this.title, this.resultGrades});

  factory SubjectResults.fromJson(Map<String, dynamic> json) => SubjectResults(
    title: json['title'],
    resultGrades: (json['resultGrades'] as List).map((i) => PeriodResult.fromJson(i)).toList(),
  );
}

class PeriodResult {
  String periodName;
  double mark;

  PeriodResult({this.periodName, this.mark});

  factory PeriodResult.fromJson(Map<String, dynamic> json) => PeriodResult(
    periodName: json['periodName'],
    mark: json['mark'] != null ? json['mark'].toDouble() : null,
  );
}