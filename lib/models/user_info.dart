import 'package:flutter/cupertino.dart';
import 'package:carousel_slider/carousel_controller.dart';

class MainUserInfo extends ChangeNotifier {
  int _id;
  List<ChildInfo> _children;
  ChildInfo _chosenChild;
  CarouselController carouselController = CarouselController();

  int get id => _id;
  set id(int newId) {
    _id = id;
    notifyListeners();
  }
  ChildInfo get chosenChild => _chosenChild;
  List<ChildInfo> get children => _children;

  void changeMainUser(int id, List<ChildInfo> children) {
    _id = id;
    _children = children;
    _chosenChild = children[0];
  }

  set children(List<ChildInfo> newChildren) {
    _children = newChildren;
    notifyListeners();
  }

  void chooseChild(int childId, bool needAnimation) {
    _chosenChild = _children.firstWhere((child) => child.id == childId);
    if (!needAnimation) carouselController.jumpToPage(_children.indexWhere((child) => child.id == childId));
    notifyListeners();
  }
}

class ChildInfo {
  int id;
  String name, avatarUrl, gender;
  List<SchoolInfo> schools;
  List<DepartmentInfo> departments;

  ChildInfo({this.id, this.name, this.avatarUrl, this.schools, this.departments, this.gender = 'Male'});
}

class SchoolInfo {
  int id;
  String name;

  SchoolInfo({this.id, this.name});
}

class DepartmentInfo {
  int id;
  String name;

  DepartmentInfo({this.id, this.name});
}

ValueNotifier mainOptions = ValueNotifier(MainUserInfo);