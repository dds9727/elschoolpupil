class News {
  int id;
  String title, subtitle, text;
  DateTime date;

  News({this.id, this.title = '', this.subtitle = '', this.text, this.date});

  factory News.fromJson(Map<String, dynamic> json) => News(
    id: json["id"],
    title: json['title'],
    subtitle: json['subtitle'],
    text: json['text'],
    date: DateTime.parse(json['date']),
  );
}
