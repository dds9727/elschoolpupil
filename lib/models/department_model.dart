class DepartmentInfo {
  int id;
  String title;
  List<String> classTutors;
  List<String> pupils;

  DepartmentInfo({this.id, this.title, this.classTutors, this.pupils});

  factory DepartmentInfo.fromJson(Map<String, dynamic> json) => DepartmentInfo(
    id: json['id'],
    title: json['title'],
    classTutors: json['classTutors'] != null ? List.from(json['classTutors']) : [],
    pupils: json['pupils'] != null ? List.from(json['pupils']) : [],
  );
}
