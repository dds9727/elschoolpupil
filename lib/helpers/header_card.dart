import 'package:flutter/material.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';

class HeaderCard extends StatelessWidget {
  final String text, subtitle;
  final Image image;
  final bool isLoading;

  HeaderCard({this.text, this.subtitle, this.image, this.isLoading = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 20, vertical: 20),
      child: Material(
        color: Colors.white,
        borderRadius: BorderRadius.circular(10),
        elevation: 8,
        shadowColor: Colors.grey.withOpacity(0.6),
        child: Container(
          height: 160,
          width: MediaQuery.of(context).size.width - 40,
          child: Stack(
            children: [
              Positioned(
                right: 30,
                top: 20,
                child: ClipRRect(
                  borderRadius: BorderRadius.circular(50),
                  child: Container(
                    height: 100,
                    width: 100,
                    color: Color(0xFFD5E0F0),
                  ),
                ),
              ),
              Positioned(
                right: 10,
                top: 10,
                child: image,
              ),
              Positioned(
                left: 25,
                top: 30,
                width: MediaQuery.of(context).size.width - 200,
                child: Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        '$subtitle',
                        style: TextStyle(
                          fontSize: 22,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                      SizedBox(height: 5),
                      isLoading ? PlaceholderWidget(height: 44, width: 75) : Text(
                        '$text',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w600,
                        ),
                        maxLines: 2,
                        overflow: TextOverflow.ellipsis,
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
