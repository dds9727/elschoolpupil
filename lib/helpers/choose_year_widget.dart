import 'package:flutter/material.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/period_model.dart';

class ChooseYearWidget extends StatelessWidget {
  final List<Period> periods;

  ChooseYearWidget({this.periods});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: false,
            floating: true,
            snap: true,
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
              'Выберите год',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: primaryColor,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate(
              [
                SizedBox(height: 20),
                ...periods.map(
                  (period) => Material(
                    color: Colors.transparent,
                    child: InkWell(
                      child: Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                        child: Text(
                          '${period.years}',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                        ),
                      ),
                      onTap: () {
                        Navigator.pop(context, period.id);
                      },
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
