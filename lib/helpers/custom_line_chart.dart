import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class SimpleTimeSeriesChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  SimpleTimeSeriesChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.TimeSeriesChart(
      seriesList,
      animate: animate,
      dateTimeFactory: const charts.LocalDateTimeFactory(),
      defaultRenderer: charts.LineRendererConfig(includeArea: true),
      domainAxis: new charts.DateTimeAxisSpec(
        tickFormatterSpec: new charts.AutoDateTimeTickFormatterSpec(
          day: new charts.TimeFormatterSpec(format: 'dd.MM', transitionFormat: 'dd.MM'),
        ),
      ),
      primaryMeasureAxis: charts.NumericAxisSpec(
        viewport: charts.NumericExtents(0.0, 5.0),
        tickProviderSpec: charts.StaticNumericTickProviderSpec([
          charts.TickSpec(1.0),
          charts.TickSpec(2.0),
          charts.TickSpec(3.0),
          charts.TickSpec(4.0),
          charts.TickSpec(5.0),
        ]),
      ),
    );
  }

  static List<charts.Series<AverageMarkByDate, DateTime>> formatAverageMarkByDateList(data) {
    return [
      new charts.Series<AverageMarkByDate, DateTime>(
        id: 'AverageMark',
        colorFn: (_, __) => charts.MaterialPalette.blue.shadeDefault,
        domainFn: (AverageMarkByDate sales, _) => sales.date,
        measureFn: (AverageMarkByDate sales, _) => sales.mark,
        data: data,
      )
    ];
  }
}

class AverageMarkByDate {
  final DateTime date;
  final double mark;

  AverageMarkByDate(this.date, this.mark);
}
