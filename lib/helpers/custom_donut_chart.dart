import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class DonutPieChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  DonutPieChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return charts.PieChart(
      seriesList,
      animate: animate,
      defaultRenderer: charts.ArcRendererConfig(arcWidth: 60),
    );
  }

  static List<charts.Series<MissedSubjectLesson, int>> formatMissedLessons(List<MissedSubjectLesson> missedSubjectLessons, List<Color> _colors) {
    List<charts.Color> colors = _colors.map((color) => charts.ColorUtil.fromDartColor(color)).toList();
    return [
      charts.Series<MissedSubjectLesson, int>(
        id: 'Sales',
        domainFn: (MissedSubjectLesson sales, _) => sales.subjectLocalId,
        measureFn: (MissedSubjectLesson sales, _) => sales.number,
        data: missedSubjectLessons,
        colorFn: (_, index) {
          return colors[index < colors.length ? index : colors.length - 1];
        },
      )
    ];
  }
}

class MissedSubjectLesson {
  final int subjectLocalId;
  final int number;

  MissedSubjectLesson(this.subjectLocalId, this.number);
}
