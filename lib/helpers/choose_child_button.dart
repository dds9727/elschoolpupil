import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/helpers/choose_child_widget.dart';

class ChooseChildButton extends StatefulWidget {
  final Function onChildChosen;

  ChooseChildButton({this.onChildChosen});

  @override
  _ChooseChildButtonState createState() => _ChooseChildButtonState();
}

class _ChooseChildButtonState extends State<ChooseChildButton> {
  @override
  Widget build(BuildContext context) {
    var chosenChild = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild);
    return ClipRRect(
      borderRadius: BorderRadius.circular(5),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
            child: Text(
              '${chosenChild.name}'.replaceAll("", "\u{200B}"),
              style: TextStyle(
                fontSize: 18,
                fontWeight: FontWeight.w600,
                color: Colors.white,
              ),
              overflow: TextOverflow.ellipsis,
            ),
          ),
          onTap: () async {
            var result = await Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => ChooseChildWidget(),
              ),
            );
            if (result != null && chosenChild.id != result) {
              var mainUserInfo = context.read<MainUserInfo>();
              mainUserInfo.chooseChild(result, false);
              widget.onChildChosen();
            }
          },
        ),
      ),
    );
  }
}
