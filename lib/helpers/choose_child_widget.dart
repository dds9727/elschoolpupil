import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/user_info.dart';

class ChooseChildWidget extends StatelessWidget {
  String joinObjectString(List<dynamic> _objects) {
    String _string = '';
    _objects.forEach((object) {
      _string += object.name + ', ';
    });
    return _string.substring(0, _string.length - 2);
  }

  @override
  Widget build(BuildContext context) {
    var mainUserInfo = context.read<MainUserInfo>();

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            pinned: false,
            floating: true,
            snap: true,
            backgroundColor: Theme.of(context).backgroundColor,
            title: Text(
              'Выберите ребёнка',
              style: TextStyle(
                color: Colors.black,
              ),
            ),
            iconTheme: IconThemeData(
              color: primaryColor,
            ),
          ),
          SliverList(
            delegate: SliverChildListDelegate([
              SizedBox(height: 20),
              ...mainUserInfo.children
                  .asMap()
                  .map(
                    (index, child) => MapEntry(
                      index,
                      Material(
                        color: Colors.transparent,
                        child: InkWell(
                          child: Container(
                            decoration: BoxDecoration(
                              border: Border(
                                top: BorderSide(
                                  width: 1,
                                  color: index == 0 ? Colors.transparent : Color(0xFFCCCCCC),
                                ),
                                left: BorderSide(
                                  width: 7,
                                  color: child.id == mainUserInfo.chosenChild.id ? primaryColor : Colors.transparent,
                                ),
                              ),
                            ),
                            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                Container(
                                  width: 60,
                                  child: Column(
                                    children: [
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(30),
                                        child: Container(
                                          color: Colors.white,
                                          height: 60,
                                          width: 60,
                                          child: Image.network(
                                            '${child.avatarUrl}',
                                            fit: BoxFit.cover,
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 15),
                                      child.id == mainUserInfo.chosenChild.id
                                          ? Text(
                                              'Текущий',
                                              style: TextStyle(
                                                color: primaryColor,
                                              ),
                                            )
                                          : Container(),
                                    ],
                                  ),
                                ),
                                SizedBox(width: 20),
                                Expanded(
                                  child: Container(
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment.start,
                                      children: [
                                        SizedBox(height: 5),
                                        Text(
                                          '${child.name}',
                                          style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.w600,
                                          ),
                                          softWrap: true,
                                        ),
                                        SizedBox(height: 10),
                                        IconTextRow(
                                          icon: Icons.people,
                                          text: '${joinObjectString(child.departments)}',
                                        ),
                                        SizedBox(height: 10),
                                        IconTextRow(
                                          icon: Icons.apartment,
                                          text: '${joinObjectString(child.schools)}',
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          onTap: () {
                            Navigator.pop(context, child.id);
                          },
                        ),
                      ),
                    ),
                  )
                  .values
                  .toList(),
              SizedBox(height: 50),
            ]),
          ),
        ],
      ),
    );
  }
}

class IconTextRow extends StatelessWidget {
  final String text;
  final IconData icon;

  IconTextRow({this.icon, this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Icon(
            icon,
            size: 26,
            color: primaryColor,
          ),
          SizedBox(width: 10),
          Expanded(
            child: Padding(
              padding: EdgeInsets.only(top: 3),
              child: Text(
                '$text',
                style: TextStyle(
                  fontSize: 16,
                ),
                softWrap: true,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
