import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:carousel_slider/carousel_slider.dart';

class ChildrenCarousel extends StatefulWidget {
  final int chosenChildId;

  ChildrenCarousel({this.chosenChildId});

  @override
  _ChildrenCarouselState createState() => _ChildrenCarouselState();
}

class _ChildrenCarouselState extends State<ChildrenCarousel> {
  @override
  Widget build(BuildContext context) {
    var mainUserInfo = context.read<MainUserInfo>();
    int _currentChildIndex = mainUserInfo.children.indexWhere((child) => child.id == widget.chosenChildId);

    return CarouselSlider(
      carouselController: mainUserInfo.carouselController,
      options: CarouselOptions(
          height: 150,
          enlargeCenterPage: true,
          enableInfiniteScroll: false,
          viewportFraction: 0.5,
          aspectRatio: 16 / 5,
          initialPage: _currentChildIndex,
          onPageChanged: (index, reason) {
            setState(() {
              mainUserInfo.chooseChild(mainUserInfo.children[index].id, true);
            });
          }),
      items: [
        ...mainUserInfo.children
            .map(
              (item) => ClipRRect(
            borderRadius: BorderRadius.circular(100),
            child: Container(
              color: Colors.white,
              height: 150,
              width: 150,
              child: Image.network(
                item.avatarUrl,
                fit: BoxFit.cover,
              ),
            ),
          ),
        )
            .toList(),
      ],
    );
  }
}
