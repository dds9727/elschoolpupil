import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class GroupedBarChart extends StatelessWidget {
  final List<charts.Series> seriesList;
  final bool animate;

  GroupedBarChart(this.seriesList, {this.animate});

  @override
  Widget build(BuildContext context) {
    return new charts.BarChart(
      seriesList,
      animate: animate,
      barGroupingType: charts.BarGroupingType.grouped,
      domainAxis: charts.OrdinalAxisSpec(
        renderSpec: charts.SmallTickRendererSpec(labelRotation: 20),
      ),
      primaryMeasureAxis: charts.NumericAxisSpec(
        viewport: charts.NumericExtents(0.0, 5.0),
        tickProviderSpec: charts.StaticNumericTickProviderSpec([
          charts.TickSpec(1.0),
          charts.TickSpec(2.0),
          charts.TickSpec(3.0),
          charts.TickSpec(4.0),
          charts.TickSpec(5.0),
        ]),
      ),
      behaviors: [
        new charts.SeriesLegend(
          desiredMaxRows: 2,
          horizontalFirst: false,
        ),
      ],
    );
  }

  static List<charts.Color> colors = [charts.ColorUtil.fromDartColor(Color(0xFF00DD66)), charts.MaterialPalette.blue.shadeDefault];

  static List<charts.Series<AverageMarkBySubject, String>> formatAverageMarkBySubject(Map<String, List<AverageMarkBySubject>> averageMarksBySubject) {
    List<charts.Series<AverageMarkBySubject, String>> list = [];
    averageMarksBySubject.forEach((key, value) {
      list.add(charts.Series<AverageMarkBySubject, String>(
        id: key,
        domainFn: (AverageMarkBySubject sales, _) => sales.subjectName,
        measureFn: (AverageMarkBySubject sales, _) => sales.mark,
        data: value,
        colorFn: (_averageMarksBySubject, _index) {
          return key == 'Средний балл ученика' ? colors[0] : colors[1];
        },
      ));
    });
    return list;
  }
}

class AverageMarkBySubject {
  final String subjectName;
  final double mark;

  AverageMarkBySubject(this.subjectName, this.mark);
}
