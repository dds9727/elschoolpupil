import 'package:flutter/material.dart';
import 'package:elschool_pupil/models/constants.dart';

class PersonAvatarPlaceholder extends StatelessWidget {
  final double size;
  PersonAvatarPlaceholder({this.size = 40});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(50),
        color: Colors.white,
        border: Border.all(width: 1, color: primaryColor),
      ),
      padding: EdgeInsets.all(5),
      child: Icon(
        Icons.person,
        size: size,
        color: primaryColor,
      ),
    );
  }
}