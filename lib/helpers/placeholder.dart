import 'package:flutter/material.dart';

class PlaceholderWidget extends StatefulWidget {
  @required
  final double height, width;
  final bool needShimmer;

  PlaceholderWidget({this.height, this.width, this.needShimmer = true});

  @override
  _PlaceholderWidgetState createState() => _PlaceholderWidgetState();
}

class _PlaceholderWidgetState extends State<PlaceholderWidget> with TickerProviderStateMixin {
  AnimationController _controller;
  Animation<double> _animation;

  @override
  void initState() {
    super.initState();
    if (widget.needShimmer) {
      _controller = AnimationController(vsync: this, duration: Duration(seconds: 1));
      _animation = Tween<double>(begin: 0.3, end: 1.0).animate(_controller)
        ..addStatusListener((status) {
          if (status == AnimationStatus.completed) {
            _controller.reverse();
          } else if (status == AnimationStatus.dismissed) {
            _controller.forward();
          }
        })
        ..addListener(() {
          setState(() {});
        });
      _controller.forward();
    }
  }

  @override
  void dispose() {
    if (widget.needShimmer) {
      _controller.dispose();
    }
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (widget.needShimmer) {
      return FadeTransition(
        opacity: _animation,
        child: Container(
          height: widget.height,
          width: widget.width,
          color: Color(0xFFDDDDDD),
        ),
      );
    } else {
      return Container(
        height: widget.height,
        width: widget.width,
        color: Color(0xFFDDDDDD),
      );
    }
  }
}
