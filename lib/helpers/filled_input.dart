import 'package:flutter/material.dart';
import 'package:elschool_pupil/models/constants.dart';

class FilledInput extends StatefulWidget {
  final Function onChanged;

  FilledInput({this.onChanged});

  @override
  _FilledInputState createState() => _FilledInputState();
}

class _FilledInputState extends State<FilledInput> {
  TextEditingController _searchController;

  @override
  void initState() {
    super.initState();
    _searchController = TextEditingController();
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: ThemeData(
        primaryColor: primaryColor,
      ),
      child: TextField(
        controller: _searchController,
        cursorHeight: 24,
        decoration: InputDecoration(
          border: OutlineInputBorder(
            borderSide: BorderSide.none,
            borderRadius: BorderRadius.all(Radius.circular(40)),
          ),
          prefixIcon: Icon(Icons.search),
          fillColor: Color(0xFFEBEBEB),
          filled: true,
          contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
        ),
        style: TextStyle(
          fontSize: 20,
        ),
        onChanged: (text) {
          widget.onChanged(text);
        },
      ),
    );
  }
}
