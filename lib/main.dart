import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/pages/profile_page.dart';
import 'package:elschool_pupil/pages/education_page.dart';
import 'package:elschool_pupil/pages/messenger_page.dart';
import 'package:elschool_pupil/pages/wallet_page.dart';
import 'package:elschool_pupil/pages/menu_page.dart';

void main() {
  runApp(
    ChangeNotifierProvider(
      create: (context) => MainUserInfo(),
      child: MyApp(),
    ),
  );
}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    precacheImage(AssetImage("assets/newspaper.png"), context);
    precacheImage(AssetImage("assets/school.png"), context);
    precacheImage(AssetImage("assets/pupils_sitting.png"), context);
    precacheImage(AssetImage("assets/boy_chill.png"), context);
    precacheImage(AssetImage("assets/girl_chill.png"), context);
    return MaterialApp(
      title: 'Elschool Diary',
      theme: ThemeData(
        primarySwatch: primaryColor,
        backgroundColor: Colors.white,
      ),
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
      ],
      supportedLocales: [
        const Locale('ru', 'RU'),
      ],
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key}) : super(key: key);

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int selectedIndex = 0;
  List<Widget> pages = [];

  void _onNavItemTapped(int index) {
    setState(() {
      selectedIndex = index;
    });
  }

  static List<Widget> bottomBarPages() {
    return <Widget>[
      const ProfilePage(),
      EducationPage(),
      const MessengerPage(),
      const WalletPageWrapper(),
      MenuPage(),
    ];
  }

  @override
  void initState() {
    Provider.of<MainUserInfo>(context, listen: false).changeMainUser(1487, [
      ChildInfo(
        id: 1488,
        name: 'Акимова Анастасия Павловна',
        gender: 'Female',
        avatarUrl: 'https://cs8.pikabu.ru/post_img/big/2017/03/02/6/1488444077142562533.jpg',
        schools: [SchoolInfo(id: 322, name: 'МБОУ Лицей №3')],
        departments: [DepartmentInfo(id: 228, name: '5Б')],
      ),
      ChildInfo(
        id: 1489,
        name: 'Акимов Илон Ривзович',
        gender: 'Male',
        avatarUrl: 'https://content.fortune.com/wp-content/uploads/2021/02/GettyImages-1229901940.jpg',
        schools: [
          SchoolInfo(id: 322, name: 'МБОУ Лицей №3'),
          SchoolInfo(id: 323, name: 'МБОУ Лицей №4'),
        ],
        departments: [DepartmentInfo(id: 228, name: '5Б')],
      ),
      ChildInfo(
        id: 1490,
        name: 'Акимов Замок Павлович',
        gender: 'Male',
        avatarUrl: 'https://m.dw.com/image/56279425_401.jpg',
        schools: [SchoolInfo(id: 322, name: 'Аквадискотека')],
        departments: [
          DepartmentInfo(id: 228, name: '5Б'),
          DepartmentInfo(id: 228, name: '13Гэ (коррекционный)'),
        ],
      ),
      ChildInfo(
        id: 1491,
        name: 'Акимова Айсидора Павловна',
        gender: 'Female',
        avatarUrl: 'https://i.imgur.com/9M6B6kD.png',
        schools: [SchoolInfo(id: 322, name: 'МБОУ Базированная Школа №1488')],
        departments: [DepartmentInfo(id: 228, name: '11А')],
      ),
      ChildInfo(
        id: 1492,
        name: 'Акимова Сидора Павловна',
        gender: 'Female',
        avatarUrl: 'https://i.pinimg.com/originals/a6/6c/75/a66c7584961f82fd8244d1adc447c431.jpg',
        schools: [SchoolInfo(id: 322, name: 'МБОУ Базированная Школа №1489')],
        departments: [DepartmentInfo(id: 228, name: '11А')],
      ),
    ]);
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([DeviceOrientation.portraitDown, DeviceOrientation.portraitUp]);
    return Scaffold(
      body: IndexedStack(
        index: selectedIndex,
        children: bottomBarPages(),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: [
          BottomNavigationBarItem(
            icon: selectedIndex == 0 ? Icon(Icons.person) : Icon(Icons.person_outline),
            label: 'Главная',
          ),
          BottomNavigationBarItem(
            icon: selectedIndex == 1 ? Icon(Icons.school) : Icon(Icons.school_outlined),
            label: 'Обучение',
          ),
          BottomNavigationBarItem(
            icon: selectedIndex == 2 ? FaIcon(FontAwesomeIcons.solidComment, size: 20) : FaIcon(FontAwesomeIcons.comment, size: 20),
            label: 'Сообщения',
          ),
          BottomNavigationBarItem(
            icon: selectedIndex == 3 ? Icon(Icons.account_balance_wallet) : Icon(Icons.account_balance_wallet_outlined),
            label: 'Счета',
          ),
          BottomNavigationBarItem(
            icon: selectedIndex == 4 ? FaIcon(FontAwesomeIcons.bars, size: 20) : FaIcon(FontAwesomeIcons.bars, size: 20),
            label: 'Меню',
          ),
        ],
        currentIndex: selectedIndex,
        type: BottomNavigationBarType.fixed,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        backgroundColor: Colors.white,
        selectedItemColor: primaryColor,
        unselectedItemColor: Colors.grey,
        onTap: _onNavItemTapped,
      ),
    );
  }
}
