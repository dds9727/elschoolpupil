import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/messenger_models.dart';
import 'package:elschool_pupil/helpers/loading_spinner.dart';
import 'package:elschool_pupil/pages/ChatPage.dart';

class MessengerPage extends StatefulWidget {
  const MessengerPage({Key key}) : super(key: key);

  @override
  _MessengerPageState createState() => _MessengerPageState();
}

class _MessengerPageState extends State<MessengerPage> {
  List<MessengerContact> messengerContacts;
  bool isLoading = true;

  void getChats() async {
    var response = await http.get(Uri.https('api.mocki.io', '/v1/e1de5a80'));
    List<MessengerContact> _messengerContacts = (json.decode(response.body) as List).map((i) => MessengerContact.fromJson(i)).toList();
    setState(() {
      messengerContacts = _messengerContacts;
      isLoading = false;
    });
  }

  @override
  void initState() {
    super.initState();
    getChats();
  }

  @override
  Widget build(BuildContext context) {
    var userId = context.select<MainUserInfo, int>((userInfo) => userInfo.id);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              title: Text(
                'Сообщения',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            !isLoading ? SliverList(
              delegate: SliverChildListDelegate(
                [
                  ...messengerContacts.map(
                    (messengerContact) => MessengerContactRow(
                      messengerContact: messengerContact,
                      isFromUser: !messengerContact.isConference && userId == messengerContact.messages.first.senderId,
                    ),
                  ),
                ],
              ),
            ) : SliverList(
              delegate: SliverChildListDelegate(
                [
                  Container(
                    height: MediaQuery.of(context).size.height - 140,
                    child: LoadingSpinner(),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessengerContactRow extends StatelessWidget {
  final MessengerContact messengerContact;
  final bool isFromUser;

  MessengerContactRow({this.messengerContact, this.isFromUser = false});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  color: Colors.white,
                  height: 60,
                  width: 60,
                  child: Image.network(
                    messengerContact.pictureUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Container(
                  height: 56,
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      Container(
                        height: 28,
                        child: Row(
                          children: [
                            Expanded(
                              child: Text(
                                messengerContact.name.replaceAll("", "\u{200B}"),
                                style: TextStyle(
                                  fontSize: 17,
                                  fontWeight: FontWeight.w600,
                                ),
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            SizedBox(width: 15),
                            Text(
                              '${DateFormat('HH:mm', 'ru-RU').format(messengerContact.messages.first.dateTime)}',
                              style: TextStyle(
                                color: Color(0xFF666666),
                              ),
                            ),
                          ],
                        ),
                      ),
                      Container(
                        height: 28,
                        child: Row(
                          children: [
                            () {
                              String prefix = '';
                              if (isFromUser) {
                                prefix = 'Вы: ';
                              } else if (messengerContact.isConference) {
                                prefix = messengerContact.messages.first.senderName + ': ';
                              }
                              return Text(
                                prefix,
                                style: TextStyle(
                                  color: Color(0xFF666666),
                                ),
                              );
                            }(),
                            Expanded(
                              child: Text(
                                messengerContact.messages.first.text.replaceAll("", "\u{200B}"),
                                style: TextStyle(
                                  fontSize: 15,
                                  color: Colors.black,
                                ),
                                softWrap: false,
                                overflow: TextOverflow.ellipsis,
                              ),
                            ),
                            () {
                              if (isFromUser) {
                                Widget _child;
                                switch (messengerContact.messages.first.messageStatus) {
                                  case 'sending':
                                    _child = Text(
                                      '...',
                                      style: TextStyle(
                                        color: primaryColor,
                                      ),
                                    );
                                    break;
                                  case 'sent':
                                    _child = FaIcon(
                                      FontAwesomeIcons.check,
                                      color: primaryColor,
                                      size: 14,
                                    );
                                    break;
                                  case 'read':
                                  default:
                                    _child = Container(
                                      width: 20,
                                      child: Stack(
                                        children: [
                                          Positioned(
                                            top: 5,
                                            left: 0,
                                            child: FaIcon(
                                              FontAwesomeIcons.check,
                                              color: primaryColor,
                                              size: 14,
                                            ),
                                          ),
                                          Positioned(
                                            top: 5,
                                            left: 6,
                                            child: FaIcon(
                                              FontAwesomeIcons.check,
                                              color: primaryColor,
                                              size: 14,
                                            ),
                                          ),
                                        ],
                                      ),
                                    );
                                    break;
                                }
                                return Container(
                                  margin: EdgeInsets.only(left: 15),
                                  child: _child,
                                );
                              } else {
                                if (messengerContact.messages.first.messageStatus == 'sent') {
                                  return Container(
                                    height: 24,
                                    margin: EdgeInsets.only(left: 15),
                                    padding: EdgeInsets.symmetric(horizontal: 8),
                                    decoration: BoxDecoration(
                                      borderRadius: BorderRadius.circular(40),
                                      color: primaryColor,
                                    ),
                                    child: Center(
                                      child: Text(
                                        '${messengerContact.unreadCounter}',
                                        style: TextStyle(
                                          color: Colors.white,
                                        ),
                                      ),
                                    ),
                                  );
                                } else {
                                  return Container();
                                }
                              }
                            }(),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
        onTap: () {
          Navigator.push(
            context,
            MaterialPageRoute(
              builder: (context) => ChatPage(
                chatId: messengerContact.chatId,
                messengerContact: messengerContact,
              ),
            ),
          );
        },
      ),
    );
  }
}
