import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:url_launcher/url_launcher.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/news_model.dart';
import 'package:elschool_pupil/helpers/loading_spinner.dart';

class NewsPage extends StatefulWidget {
  final News news;

  NewsPage({this.news});

  @override
  _NewsPageState createState() => _NewsPageState();
}

class _NewsPageState extends State<NewsPage> {
  @override
  void initState() {
    super.initState();
    if (widget.news.text == null) {
      getNewsText();
    } else {
      setState(() {
        isLoading = false;
      });
    }
  }

  bool isLoading = true;

  void getNewsText() async {
    String url = '/v1/8412ec9d';
    if (widget.news.id == 4) {
      url = '/v1/a0428c5a';
    } else if (widget.news.id == 5) {
      url = '/v1/8412ec9d';
    } else {
      url = '/v1/74e81a73';
    }
    var response = await http.get(Uri.https('api.mocki.io', url));
    String _text = json.decode(response.body)['text'];
    if (mounted) setState(() {
      widget.news.text = _text;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              iconTheme: IconThemeData(
                color: primaryColor,
              ),
              title: Text(
                'Новость',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      '${widget.news.title}',
                      style: TextStyle(
                        color: primaryColor,
                        fontSize: 22,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      '${widget.news.date}',
                      style: TextStyle(
                        fontSize: 14,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 20),
                    child: Text(
                      '${widget.news.subtitle}',
                      style: TextStyle(
                        fontSize: 18,
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  !isLoading
                      ? Container(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                          child: Html(
                            data: widget.news.text,
                            onLinkTap: (url) {
                              if (url != null) {
                                launch(url);
                              } else {
                                print('Обработай ошибку');
                              }
                            },
                          ),
                        )
                      : Container(
                          width: MediaQuery.of(context).size.width,
                          height: 200,
                          child: LoadingSpinner(),
                        ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
