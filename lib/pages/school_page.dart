import 'package:flutter/material.dart';
import 'dart:math';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/school_model.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/header_card.dart';

class SchoolPage extends StatefulWidget {
  final int schoolId;

  SchoolPage({this.schoolId});

  @override
  _SchoolPageState createState() => _SchoolPageState();
}

class _SchoolPageState extends State<SchoolPage> {
  SchoolInfo schoolInfo = SchoolInfo(
    id: 3229,
    name: '',
    districtName: '',
    phone: '',
    principleName: '',
    address: '',
    email: '',
  );
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    getSchoolInfo();
  }

  void getSchoolInfo() async {
    var response = await http.get(Uri.https('api.mocki.io', 'v1/377435ef'));
    if (mounted) setState(() {
      schoolInfo = SchoolInfo.fromJson(json.decode(response.body));
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              iconTheme: IconThemeData(
                color: primaryColor,
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  HeaderCard(
                    subtitle: 'Образовательное учреждение',
                    image: Image.asset('assets/school.png', height: 150),
                    text: '',
                  ),
                  SizedBox(height: 15),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 25),
                    child: isLoading
                        ? PlaceholderWidget(width: MediaQuery.of(context).size.width, height: 100)
                        : Text(
                            '${schoolInfo.name}',
                            style: TextStyle(
                              fontSize: 22,
                            ),
                          ),
                  ),
                  SizedBox(height: 30),
                  InfoRow(
                    icon: Icons.person,
                    text: '${schoolInfo.principleName}',
                    subtitle: 'Директор',
                    isLoading: isLoading,
                  ),
                  SizedBox(height: 30),
                  InfoRow(
                    icon: Icons.school,
                    text: '${schoolInfo.districtName}',
                    subtitle: 'Отдел образования',
                    isLoading: isLoading,
                  ),
                  SizedBox(height: 70),
                  InfoRow(
                    icon: Icons.phone,
                    text: '${schoolInfo.phone}',
                    subtitle: 'Телефон',
                    isLoading: isLoading,
                  ),
                  SizedBox(height: 30),
                  InfoRow(
                    icon: Icons.mail,
                    text: '${schoolInfo.email}',
                    subtitle: 'E-mail',
                    isLoading: isLoading,
                  ),
                  SizedBox(height: 30),
                  InfoRow(
                    icon: Icons.location_on,
                    text: '${schoolInfo.address}',
                    subtitle: 'Адрес',
                    isLoading: isLoading,
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class InfoRow extends StatelessWidget {
  final IconData icon;
  final String text, subtitle;
  final bool isLoading;

  InfoRow({this.icon, this.text, this.subtitle, this.isLoading = false});

  @override
  Widget build(BuildContext context) {
    var rng = new Random();
    return Container(
      width: MediaQuery.of(context).size.width,
      padding: EdgeInsets.symmetric(horizontal: 25),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Padding(
            padding: EdgeInsets.only(top: 3),
            child: Icon(
              icon,
              size: 40,
              color: primaryColor,
            ),
          ),
          SizedBox(width: 20),
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '$subtitle',
                    style: TextStyle(
                      fontSize: 14,
                      color: Color(0xFF666666),
                    ),
                  ),
                  SizedBox(height: 5),
                  isLoading
                      ? PlaceholderWidget(width: 150 + rng.nextInt(100).toDouble(), height: 20)
                      : Text(
                          '$text',
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          softWrap: true,
                        ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }
}
