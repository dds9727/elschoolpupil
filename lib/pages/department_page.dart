import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/department_model.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/person_avatar_placeholder.dart';
import 'package:elschool_pupil/helpers/header_card.dart';

class DepartmentPage extends StatefulWidget {
  final int departmentId;

  DepartmentPage({this.departmentId});

  @override
  _DepartmentPageState createState() => _DepartmentPageState();
}

class _DepartmentPageState extends State<DepartmentPage> {
  DepartmentInfo departmentInfo = DepartmentInfo(
    id: 1,
    title: '',
    classTutors: [],
    pupils: [],
  );
  bool isLoading = true;

  @override
  void initState() {
    super.initState();
    getDepartmentInfo();
  }

  void getDepartmentInfo() async {
    var response = await http.get(Uri.https('api.mocki.io', '/v1/caf4705a'));
    if (mounted) setState(() {
      departmentInfo = DepartmentInfo.fromJson(json.decode(response.body));
      departmentInfo.pupils.sort((a, b) => a.toLowerCase().compareTo(b.toLowerCase()));
      isLoading = false;
    });
  }

  List<Widget> pupilPlaceholders(bool isLoading) {
    if (isLoading) {
      return [
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            children: [
              PersonAvatarPlaceholder(),
              SizedBox(width: 15),
              Container(
                width: MediaQuery.of(context).size.width - 107,
                child: PlaceholderWidget(width: 200, height: 20),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            children: [
              PersonAvatarPlaceholder(),
              SizedBox(width: 15),
              Container(
                width: MediaQuery.of(context).size.width - 107,
                child: PlaceholderWidget(width: 220, height: 20),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            children: [
              PersonAvatarPlaceholder(),
              SizedBox(width: 15),
              Container(
                width: MediaQuery.of(context).size.width - 107,
                child: PlaceholderWidget(width: 180, height: 20),
              ),
            ],
          ),
        ),
        Container(
          padding: EdgeInsets.symmetric(horizontal: 20),
          margin: EdgeInsets.only(bottom: 20),
          child: Row(
            children: [
              PersonAvatarPlaceholder(),
              SizedBox(width: 15),
              Container(
                width: MediaQuery.of(context).size.width - 107,
                child: PlaceholderWidget(width: 200, height: 20),
              ),
            ],
          ),
        ),
      ];
    } else {
      return [];
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              iconTheme: IconThemeData(
                color: primaryColor,
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                  [
                    HeaderCard(
                      text: '${departmentInfo.title != null ? departmentInfo.title : ''}',
                      subtitle: 'Класс',
                      image: Image.asset('assets/pupils_sitting.png', height: 150),
                      isLoading: isLoading,
                    ),
                    SizedBox(height: 20),
                    isLoading
                        ? Container(
                      width: MediaQuery.of(context).size.width,
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      margin: EdgeInsets.only(bottom: 20),
                      child: Row(
                        children: [
                          PersonAvatarPlaceholder(size: 50),
                          SizedBox(width: 15),
                          Expanded(
                            child: Container(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: [
                                  Text(
                                    'Классный руководитель',
                                    style: TextStyle(
                                      color: Color(0xFF666666),
                                    ),
                                  ),
                                  SizedBox(height: 5),
                                  PlaceholderWidget(width: 200, height: 24),
                                ],
                              ),
                            ),
                          ),
                        ],
                      ),
                    )
                        : Container(),
                    ...departmentInfo.classTutors.map(
                          (tutor) => Container(
                        width: MediaQuery.of(context).size.width,
                        padding: EdgeInsets.symmetric(horizontal: 20),
                        margin: EdgeInsets.only(bottom: 20),
                        child: Row(
                          children: [
                            PersonAvatarPlaceholder(size: 50),
                            SizedBox(width: 15),
                            Expanded(
                              child: Container(
                                child: Column(
                                  crossAxisAlignment: CrossAxisAlignment.start,
                                  children: [
                                    Text(
                                      'Классный руководитель',
                                      style: TextStyle(
                                        color: Color(0xFF666666),
                                      ),
                                    ),
                                    SizedBox(height: 5),
                                    Text(
                                      '$tutor'.replaceAll("", "\u{200B}"),
                                      style: TextStyle(fontSize: 20),
                                      overflow: TextOverflow.ellipsis,
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 30),
                    Padding(
                      padding: EdgeInsets.symmetric(horizontal: 20),
                      child: Text(
                        'Список учащихся ${departmentInfo.pupils != null ? '(${departmentInfo.pupils.length})' : ''}',
                        style: TextStyle(
                          fontSize: 20,
                          color: Color(0xFF666666),
                        ),
                      ),
                    ),
                    SizedBox(height: 20),
                    ...pupilPlaceholders(isLoading),
                    ...departmentInfo.pupils
                        .asMap()
                        .map(
                          (index, pupil) => MapEntry(
                        index,
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          margin: EdgeInsets.only(bottom: 20),
                          child: Row(
                            children: [
                              PersonAvatarPlaceholder(),
                              SizedBox(width: 15),
                              Container(
                                width: MediaQuery.of(context).size.width - 107,
                                child: Text(
                                  '${index + 1}. $pupil'.replaceAll("", "\u{200B}"),
                                  style: TextStyle(fontSize: 16),
                                  overflow: TextOverflow.ellipsis,
                                ),
                              ),
                            ],
                          ),
                        ),
                      ),
                    )
                        .values
                        .toList(),
                    SizedBox(height: 50),
                  ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}