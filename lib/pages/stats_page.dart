import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/custom_group_chart.dart' as custom_group_charts;
import 'package:elschool_pupil/helpers/custom_line_chart.dart' as custom_line_charts;
import 'package:elschool_pupil/helpers/custom_donut_chart.dart' as custom_donut_chart;


const List<Color> chartColors = [
  (Colors.red),
  (Colors.blue),
  (Colors.pink),
  (Colors.cyan),
  (Colors.deepPurple),
  (Colors.green),
  (Colors.indigo),
  (Colors.orange),
  (Colors.teal),
  (Colors.purple),
];

class StatsPage extends StatefulWidget {
  @override
  _StatsPageState createState() => _StatsPageState();
}

class _StatsPageState extends State<StatsPage> {
  int weekNumber;
  double averageMark = 0;

  Map<String, Color> averageColor(double mark) {
    Map<String, Color> _colors = {'backgroundColor': Color(0xFF98FB98).withOpacity(0.3), 'color': Color(0xFF98FB98)};
    if (mark >= 4.5) {
      _colors['backgroundColor'] = Color(0xFF98FB98).withOpacity(0.3);
      _colors['color'] = Color(0xFF88EB88);
    } else if (mark >= 3.5) {
      _colors['backgroundColor'] = Color(0xFF87CEEB).withOpacity(0.3);
      _colors['color'] = Color(0xFF87CEEB);
    } else if (mark >= 2.5) {
      _colors['backgroundColor'] = Color(0xFFF0E68C).withOpacity(0.45);
      _colors['color'] = Color(0xFFE0D67C);
    } else if (mark > 0) {
      _colors['backgroundColor'] = Color(0xFFFFB6C1).withOpacity(0.3);
      _colors['color'] = Color(0xFFEFA6B1);
    } else {
      _colors['backgroundColor'] = Color(0xFFCCCCCC).withOpacity(0.3);
      _colors['color'] = Color(0xFFCCCCCC);
    }
    return _colors;
  }

  Map<DateTime, double> averageLineChart = {
    DateTime.parse('2021-02-13'): 3.4,
    DateTime.parse('2021-02-14'): 3.7,
    DateTime.parse('2021-02-15'): 4.1,
    DateTime.parse('2021-02-16'): 3.5,
    DateTime.parse('2021-02-17'): 3.9,
    DateTime.parse('2021-02-18'): 4.2,
  };

  List<custom_line_charts.AverageMarkByDate> averageMarkForPeriod = [];

  Map<String, List<custom_group_charts.AverageMarkBySubject>> averageMarksBySubject = {
    'Средний балл ученика': [],
    'Средний балл по классу': [],
  };

  List<MissedSubjectLessonsCount> missedSubjectLessonCounts = [];
  List<custom_donut_chart.MissedSubjectLesson> missedSubjectLessons = [];

  List<MissedLesson> missedLessons = [];

  bool generalDataLoading = true, chartsLoading = true;

  @override
  void initState() {
    super.initState();
    getGeneralStatsData();
    getChartsData();
  }

  void getGeneralStatsData() async {
    await new Future.delayed(const Duration(milliseconds: 500));
    setState(() {
      weekNumber = 25;
      averageMark = 4.9;
      generalDataLoading = false;
    });
  }

  List<Widget> drawMissedLessonSubjectChips() {
    int _index = -1;
    List<Widget> _list = [];
    _list.add(Container(
      child: Row(
        children: [
          SizedBox(width: 15),
          ...missedSubjectLessons.where((item) => item.subjectLocalId < missedSubjectLessons.length / 3).toList().map((item) {
            _index++;
            MissedSubjectLessonsCount count = missedSubjectLessonCounts[_index];
            return ChartChip(
              subjectName: count.subjectName,
              count: count.count,
              color: chartColors[_index < chartColors.length ? _index : chartColors.length - 1],
            );
          }),
        ],
      ),
    ));
    _list.add(SizedBox(height: 10));
    _list.add(Container(
      child: Row(
        children: [
          SizedBox(width: 15),
          ...missedSubjectLessons.where((item) => (item.subjectLocalId < 2 * missedSubjectLessons.length / 3) && (item.subjectLocalId >= 1 / 3 * missedSubjectLessons.length)).toList().map((item) {
            _index++;
            MissedSubjectLessonsCount count = missedSubjectLessonCounts[_index];
            return ChartChip(
              subjectName: count.subjectName,
              count: count.count,
              color: chartColors[_index < chartColors.length ? _index : chartColors.length - 1],
            );
          }),
        ],
      ),
    ));
    _list.add(SizedBox(height: 10));
    _list.add(Container(
      child: Row(
        children: [
          SizedBox(width: 15),
          ...missedSubjectLessons.where((item) => (item.subjectLocalId < missedSubjectLessons.length) && (item.subjectLocalId >= 2 / 3 * missedSubjectLessons.length)).toList().map((item) {
            _index++;
            MissedSubjectLessonsCount count = missedSubjectLessonCounts[_index];
            return ChartChip(
              subjectName: count.subjectName,
              count: count.count,
              color: chartColors[_index < chartColors.length ? _index : chartColors.length - 1],
            );
          }),
        ],
      ),
    ));
    return _list;
  }

  void getChartsData() async {
    await new Future.delayed(const Duration(milliseconds: 3000));
    if (mounted)
      setState(() {
        averageMarkForPeriod = [
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 13), 3.4),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 14), 3.7),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 15), 4.1),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 16), 3.5),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 17), 3.9),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 18), 4.2),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 19), 4.4),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 20), 4.4),
          custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 21), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 22), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 23), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 24), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 25), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 26), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 27), 4.4),
          // custom_line_charts.AverageMarkByDate(DateTime(2021, 02, 28), 4.4),
        ];

        averageMarksBySubject['Средний балл ученика'] = [
          custom_group_charts.AverageMarkBySubject('Математика', 4.1),
          custom_group_charts.AverageMarkBySubject('Биология', 4.75),
          custom_group_charts.AverageMarkBySubject('Физ. кул.', 5),
          custom_group_charts.AverageMarkBySubject('Технология', 4.8),
          custom_group_charts.AverageMarkBySubject('География', 4.8),
          custom_group_charts.AverageMarkBySubject('ИЗО', 5),
          custom_group_charts.AverageMarkBySubject('Музыка', 4.7),
          custom_group_charts.AverageMarkBySubject('Англ. яз.', 4.3),
          custom_group_charts.AverageMarkBySubject('История', 4.7),
          custom_group_charts.AverageMarkBySubject('Культура Башкортостана', 5),
          custom_group_charts.AverageMarkBySubject('Литература', 5),
          custom_group_charts.AverageMarkBySubject('Башкирский язык', 5),
        ];
        averageMarksBySubject['Средний балл по классу'] = [
          custom_group_charts.AverageMarkBySubject('Математика', 4.92),
          custom_group_charts.AverageMarkBySubject('Биология', 4.95),
          custom_group_charts.AverageMarkBySubject('Физ. кул.', 5),
          custom_group_charts.AverageMarkBySubject('Технология', 5),
          custom_group_charts.AverageMarkBySubject('География', 5),
          custom_group_charts.AverageMarkBySubject('ИЗО', 5),
          custom_group_charts.AverageMarkBySubject('Музыка', 4.4),
          custom_group_charts.AverageMarkBySubject('Англ. яз.', 4.65),
          custom_group_charts.AverageMarkBySubject('История', 4.9),
          custom_group_charts.AverageMarkBySubject('Культура Башкортостана', 5),
          custom_group_charts.AverageMarkBySubject('Литература', 5),
          custom_group_charts.AverageMarkBySubject('Башкирский язык', 5),
        ];

        missedSubjectLessonCounts = [
          MissedSubjectLessonsCount(subjectName: 'Труд', count: 2),
          MissedSubjectLessonsCount(subjectName: 'История', count: 2),
          MissedSubjectLessonsCount(subjectName: 'Английский язык', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Русский язык', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Физкультура', count: 1),
          MissedSubjectLessonsCount(subjectName: 'География', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Математика', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Культура Башкортостана', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Литература', count: 1),
          MissedSubjectLessonsCount(subjectName: 'Музыка', count: 1),
        ];
        int index = -1;
        missedSubjectLessons = missedSubjectLessonCounts.map((count) {
          index++;
          return custom_donut_chart.MissedSubjectLesson(index, count.count);
        }).toList();

        missedLessons = [
          MissedLesson(theme: 'Окончательное оформление изделия', subjectName: 'Технология', date: DateTime(2021, 02, 16)),
          MissedLesson(theme: 'Защита творческого проекта', subjectName: 'Технология', date: DateTime(2021, 02, 16)),
          MissedLesson(theme: 'В гаванях афинского порта Пирей', subjectName: 'История', date: DateTime(2021, 02, 16)),
          MissedLesson(theme: '', subjectName: 'Английский язык', date: DateTime(2021, 02, 17)),
          MissedLesson(theme: 'Всякие буквы', subjectName: 'Русский язык', date: DateTime(2021, 02, 17)),
          MissedLesson(theme: 'В городе богини Афины', subjectName: 'История', date: DateTime(2021, 02, 18)),
          MissedLesson(theme: 'Повторение изученного в разделе "Морфемика. Орфография. Культура речи"', subjectName: 'Русский язык', date: DateTime(2021, 02, 18)),
        ];
        chartsLoading = false;
      });
  }

  @override
  Widget build(BuildContext context) {
    var mainUserInfo = context.watch<MainUserInfo>();
    var _averageMarkColors = averageColor(averageMark);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              iconTheme: IconThemeData(
                color: primaryColor,
              ),
              title: Text(
                'Статистика',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate([
                SizedBox(height: 20),
                Center(
                  child: ClipRRect(
                    borderRadius: BorderRadius.circular(100),
                    child: Container(
                      color: Colors.white,
                      height: 100,
                      width: 100,
                      child: Image.network(
                        '${mainUserInfo.chosenChild.avatarUrl}',
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ),
                SizedBox(height: 10),
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width,
                  child: Text(
                    '${mainUserInfo.chosenChild.name}',
                    style: TextStyle(
                      fontSize: 18,
                      fontWeight: FontWeight.w500,
                    ),
                    softWrap: true,
                    textAlign: TextAlign.center,
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  child: Row(
                    children: [
                      SizedBox(width: 20),
                      Expanded(
                        child: IconCard(
                          title: 'Неделя',
                          text: '$weekNumber',
                          icon: Icons.calendar_today_outlined,
                          color: Color(0xFF705BCF),
                          backgroundColor: Color(0xFFEDEAFF),
                          loading: generalDataLoading,
                        ),
                      ),
                      SizedBox(width: 20),
                      Expanded(
                        child: IconCard(
                          title: 'Средний общий балл',
                          text: '$averageMark',
                          icon: Icons.show_chart,
                          color: _averageMarkColors['color'],
                          backgroundColor: _averageMarkColors['backgroundColor'],
                          loading: generalDataLoading,
                        ),
                      ),
                      SizedBox(width: 20),
                    ],
                  ),
                ),
                SizedBox(height: 20),
                Container(
                  width: MediaQuery
                      .of(context)
                      .size
                      .width - 40,
                  margin: EdgeInsets.symmetric(horizontal: 20),
                  padding: EdgeInsets.symmetric(vertical: 20),
                  decoration: BoxDecoration(
                    color: Colors.white,
                    border: Border.all(
                      width: 1,
                      color: Color(0xFFDDDDDD),
                    ),
                    borderRadius: BorderRadius.circular(10),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Text(
                          '3 четверть',
                          style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.w500,
                          ),
                        ),
                      ),
                      SizedBox(height: 30),
                      ChartTitle('Средний балл за учебный период'),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: chartsLoading
                            ? PlaceholderWidget(height: 16, width: 150)
                            : (averageMarkForPeriod.length > 0
                            ? Text('С ${DateFormat('dd.MM', 'ru-RU').format(averageMarkForPeriod[0].date)} по ${DateFormat('dd.MM', 'ru-RU').format(averageMarkForPeriod.last.date)}')
                            : Text('')),
                      ),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 200,
                        child: chartsLoading
                            ? PlaceholderWidget(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: 200,
                        )
                            : (averageMarkForPeriod.length > 0
                            ? custom_line_charts.SimpleTimeSeriesChart(
                          custom_line_charts.SimpleTimeSeriesChart.formatAverageMarkByDateList(averageMarkForPeriod),
                          animate: true,
                        )
                            : ChartAlert('Недостаточно данных')),
                      ),
                      SizedBox(height: 50),
                      ChartTitle('Средний балл по предметам'),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.only(left: 15),
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 300,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            width: (averageMarksBySubject['Средний балл ученика'].length * 90.0 - 72) > (MediaQuery
                                .of(context)
                                .size
                                .width - 72)
                                ? (averageMarksBySubject['Средний балл ученика'].length * 90.0 - 72)
                                : (MediaQuery
                                .of(context)
                                .size
                                .width - 72),
                            height: 300,
                            child: chartsLoading
                                ? PlaceholderWidget(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              height: 200,
                            )
                                : (averageMarksBySubject['Средний балл ученика'].length > 0
                                ? custom_group_charts.GroupedBarChart(
                              custom_group_charts.GroupedBarChart.formatAverageMarkBySubject(averageMarksBySubject),
                              animate: true,
                            )
                                : ChartAlert('Недостаточно данных')),
                          ),
                        ),
                      ),
                      SizedBox(height: 50),
                      ChartTitle('Пропуски по предметам'),
                      SizedBox(height: 10),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        width: MediaQuery
                            .of(context)
                            .size
                            .width,
                        height: 200,
                        child: chartsLoading
                            ? PlaceholderWidget(
                          width: MediaQuery
                              .of(context)
                              .size
                              .width,
                          height: 200,
                        )
                            : (missedSubjectLessons.length > 0
                            ? custom_donut_chart.DonutPieChart(
                          custom_donut_chart.DonutPieChart.formatMissedLessons(missedSubjectLessons, chartColors),
                          animate: true,
                        )
                            : NoMissedLessonsBlock('Ура! Нет пропусков уроков')),
                      ),
                      Visibility(
                        visible: missedSubjectLessons.length > 0,
                        child: SingleChildScrollView(
                          scrollDirection: Axis.horizontal,
                          child: Container(
                            height: 100,
                            child: Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: drawMissedLessonSubjectChips(),
                            ),
                          ),
                        ),
                      ),
                      SizedBox(height: 50),
                      ChartTitle('Пропущенные темы'),
                      SizedBox(height: 20),
                      Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          children: [
                            chartsLoading
                                ? PlaceholderWidget(
                              width: MediaQuery
                                  .of(context)
                                  .size
                                  .width,
                              height: 200,
                            )
                                : Container(),
                            !chartsLoading && missedLessons.length == 0 ? NoMissedLessonsBlock('Ура! Пропущенных тем нет') : Container(),
                            ...missedLessons.map(
                                  (lesson) => MissedLessonRow(lesson: lesson),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 50),
              ],),
            ),
          ],
        ),
      ),
    );
  }
}

class IconCard extends StatelessWidget {
  final String title, text;
  final IconData icon;
  final Color color, backgroundColor;
  final bool loading;

  IconCard({this.title = '', this.text = '', this.icon, this.color, this.backgroundColor, this.loading = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      height: 130,
      decoration: BoxDecoration(
        color: backgroundColor,
        borderRadius: BorderRadius.circular(10),
      ),
      child: Stack(
        children: [
          Positioned(
            right: 10,
            top: 30,
            child: Opacity(
              opacity: 0.6,
              child: Icon(
                icon,
                color: color,
                size: 70,
              ),
            ),
          ),
          Positioned(
            bottom: 15,
            left: 15,
            width: MediaQuery.of(context).size.width / 2 - 60,
            height: 100,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  '$title',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.65),
                    fontSize: 18,
                    fontWeight: FontWeight.w600,
                  ),
                ),
                Expanded(child: Container()),
                loading
                    ? PlaceholderWidget(
                        width: 60,
                        height: 42,
                      )
                    : Text(
                        '$text',
                        style: TextStyle(
                          fontSize: 40,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ChartTitle extends StatelessWidget {
  final String title;

  ChartTitle(this.title);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Text(
        '$title',
        style: TextStyle(
          fontSize: 20,
        ),
      ),
    );
  }
}

class MissedSubjectLessonsCount {
  String subjectName;
  int count;

  MissedSubjectLessonsCount({this.subjectName, this.count});
}

class ChartChip extends StatelessWidget {
  final Color color;
  final String subjectName;
  final int count;

  ChartChip({this.subjectName, this.count, this.color});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: color,
        borderRadius: BorderRadius.circular(15),
      ),
      margin: EdgeInsets.only(right: 10),
      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
      child: Text(
        '$subjectName $count',
        style: TextStyle(color: Colors.white),
        softWrap: false,
        overflow: TextOverflow.fade,
      ),
    );
  }
}

class MissedLesson {
  String subjectName, theme;
  DateTime date;

  MissedLesson({this.subjectName, this.theme, this.date});
}

class MissedLessonRow extends StatelessWidget {
  final MissedLesson lesson;

  MissedLessonRow({this.lesson});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            child: Container(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text(
                    '${lesson.subjectName}',
                    style: TextStyle(
                      fontSize: 16,
                      color: Colors.black54,
                    ),
                  ),
                  Text(
                    '${lesson.theme}',
                    style: TextStyle(fontSize: 16),
                  ),
                ],
              ),
            ),
          ),
          SizedBox(width: 10),
          Text(
            '${DateFormat('dd.MM', 'ru-RU').format(lesson.date)}',
            style: TextStyle(
              fontSize: 16,
            ),
          ),
        ],
      ),
    );
  }
}

class ChartAlertWrapper extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container();
  }
}

class ChartAlert extends StatelessWidget {
  final String text;

  ChartAlert(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFFFF1E1),
        borderRadius: BorderRadius.circular(15),
      ),
      height: 170,
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.error_outline,
              size: 80,
              color: Color(0xFFFFD29D),
            ),
            SizedBox(height: 15),
            Text(
              '$text',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class NoMissedLessonsBlock extends StatelessWidget {
  final String text;

  NoMissedLessonsBlock(this.text);

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        color: Color(0xFFDDF0F9),
        borderRadius: BorderRadius.circular(15),
      ),
      height: 170,
      margin: EdgeInsets.symmetric(vertical: 15),
      child: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Icon(
              Icons.thumb_up,
              size: 80,
              color: Color(0xFF82B4CC),
            ),
            SizedBox(height: 15),
            Text(
              '$text',
              style: TextStyle(
                fontSize: 18,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
