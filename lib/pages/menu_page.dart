import 'package:flutter/material.dart';

class MenuPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        title: Text(
          'Меню',
          style: TextStyle(
            color: Colors.black,
          ),
        ),
        elevation: 0,
      ),
      body: Container(
        child: Column(
          children: [
            SizedBox(height: 20),
            MenuTile(
              icon: Icons.person,
              text: 'Настройки профиля',
              onTap: () {},
            ),
            MenuTile(
              icon: Icons.settings,
              text: 'Настройки приложения',
              onTap: () {},
            ),
            MenuTile(
              icon: Icons.info,
              text: 'О приложении',
              onTap: () {},
            ),
            Expanded(
              child: Container(),
            ),
            MenuTile(
              icon: Icons.exit_to_app,
              text: 'Выйти',
              color: Color(0xFFDD1122),
              onTap: () {},
            ),
          ],
        ),
      ),
    );
  }
}

class MenuTile extends StatelessWidget {
  final String text;
  final IconData icon;
  final Function onTap;
  final Color color;

  MenuTile({this.icon, this.text, this.onTap, this.color = Colors.black});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.white,
      child: InkWell(
        highlightColor: color.withOpacity(0.1),
        splashColor: color.withOpacity(0.1),
        child: Container(
          padding: EdgeInsets.symmetric(horizontal: 25, vertical: 15),
          child: Row(
            children: [
              Icon(
                icon,
                color: color.withOpacity(0.5),
              ),
              SizedBox(width: 20),
              Text(
                '$text',
                style: TextStyle(
                  color: color,
                  fontSize: 16,
                ),
              ),
            ],
          ),
        ),
        onTap: onTap,
      ),
    );
  }
}
