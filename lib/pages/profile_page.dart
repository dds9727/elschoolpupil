import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/news_model.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/children_carousel.dart';
import 'package:elschool_pupil/pages/news_page.dart';

class ProfilePage extends StatefulWidget {
  const ProfilePage({Key key}) : super(key: key);

  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfilePage> {
  List<News> news = [];

  bool isLoading = true;
  List<Widget> carouselSliderItems = [];

  @override
  void initState() {
    super.initState();
    getNews();
  }

  void getNews() async {
    var response = await http.get(Uri.https('api.mocki.io', '/v1/12f22d1e'));
    // var response = await http.get(Uri.https('api.mocki.io', '/v1/68c0ed3e'));
    List<News> _news = (json.decode(response.body) as List).map((i) => News.fromJson(i)).toList();
    setState(() {
      news = _news;
      isLoading = false;
    });
  }

  @override
  Widget build(BuildContext context) {
    var _chosenChild = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Профиль'),
            pinned: true,
            floating: true,
            snap: true,
            expandedHeight: 250,
            // systemOverlayStyle: SystemUiOverlayStyle(statusBarColor: Colors.orange),
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.none,
              background: Column(
                mainAxisAlignment: MainAxisAlignment.end,
                children: [
                  ChildrenCarousel(chosenChildId: _chosenChild.id),
                  SizedBox(height: 10),
                  Padding(
                    padding: EdgeInsets.symmetric(horizontal: 15),
                    child: Text(
                      '${_chosenChild.name.replaceAll("", "\u{200B}")}',
                      overflow: TextOverflow.ellipsis,
                      softWrap: true,
                      style: TextStyle(
                        color: Colors.white,
                        fontSize: 20,
                        fontWeight: FontWeight.w500,
                      ),
                    ),
                  ),
                  SizedBox(height: 20),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                News _thisNews = news[index];
                return InkWell(
                  child: Container(
                    margin: EdgeInsets.only(left: 25, right: 25, top: index == 0 ? 20 : 10, bottom: index == news.length - 1 ? 50 : 10),
                    child: NewsCard(
                      title: _thisNews.title,
                      text: _thisNews.subtitle,
                      dateTime: _thisNews.date,
                    ),
                  ),
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(builder: (context) => NewsPage(news: news[index])),
                    );
                  },
                );
              },
              childCount: news.length,
            ),
          ),
          isLoading
              ? SliverList(
                  delegate: SliverChildBuilderDelegate(
                    (BuildContext context, int index) => Container(
                      margin: EdgeInsets.only(left: 25, right: 25, top: index == 0 ? 20 : 10, bottom: index == news.length - 1 ? 50 : 10),
                      child: NewsCardPlaceholder(),
                    ),
                    childCount: 3,
                  ),
                )
              : SliverList(
                  delegate: SliverChildListDelegate(
                    [
                      (news.length == 0 && !isLoading)
                          ? Container(
                              height: MediaQuery.of(context).size.height - 330,
                              child: Center(
                                child: Column(
                                  mainAxisAlignment: MainAxisAlignment.center,
                                  children: [
                                    Container(
                                      width: MediaQuery.of(context).size.width - 200,
                                      child: Image.asset('assets/newspaper.png'),
                                    ),
                                    Text(
                                      'Новостей нет',
                                      style: TextStyle(
                                        color: Colors.black54,
                                        fontSize: 24,
                                        fontWeight: FontWeight.w600,
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            )
                          : Container(),
                    ],
                  ),
                ),
        ],
      ),
    );
  }
}

class NewsCard extends StatelessWidget {
  final String title, text;
  final DateTime dateTime;

  NewsCard({this.title, this.text, this.dateTime});

  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: cardColor,
      ),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            title,
            style: TextStyle(
              color: primaryColor,
              fontSize: 17,
            ),
          ),
          SizedBox(height: 10),
          Text(
            text,
            style: TextStyle(
              fontSize: 17,
            ),
          ),
          SizedBox(height: 15),
          Text(
            DateFormat('dd.MM.yyyy').format(dateTime),
            style: TextStyle(
              fontSize: 17,
            ),
          ),
        ],
      ),
    );
  }
}

class NewsCardPlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Container(
      decoration: BoxDecoration(
        borderRadius: BorderRadius.circular(10),
        color: cardColor,
      ),
      padding: EdgeInsets.symmetric(vertical: 20, horizontal: 20),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          PlaceholderWidget(width: 150, height: 19),
          SizedBox(height: 10),
          PlaceholderWidget(width: MediaQuery.of(context).size.width, height: 80),
          SizedBox(height: 15),
          PlaceholderWidget(width: 100, height: 19),
        ],
      ),
    );
  }
}