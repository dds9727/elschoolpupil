import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:intl/intl.dart';
import 'package:pie_chart/pie_chart.dart';
import 'package:provider/provider.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/wallet_model.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/helpers/loading_spinner.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/pages/transaction_page.dart';

class BalanceConstant {
  String keyword, ruName;
  IconData icon;
  Color color;

  BalanceConstant({this.keyword, this.ruName, this.icon, this.color});
}

Map<String, BalanceConstant> balanceConstants = {
  'Dining': BalanceConstant(keyword: 'Dining', ruName: 'Столовая', icon: Icons.local_dining, color: Color(0xFF3697C1)),
  'Buffet': BalanceConstant(keyword: 'Buffet', ruName: 'Буфет', icon: Icons.local_cafe, color: Color(0xFFf06292)),
  'Alga': BalanceConstant(keyword: 'Alga', ruName: 'Алга', icon: Icons.credit_card, color: Color(0xFFffb74d)),
};

class WalletPageWrapper extends StatefulWidget {
  const WalletPageWrapper({Key key}) : super(key: key);

  @override
  _WalletPageWrapperState createState() => _WalletPageWrapperState();
}

class _WalletPageWrapperState extends State<WalletPageWrapper> {
  @override
  Widget build(BuildContext context) {
    final _chosenChildId = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild).id;
    return FutureBuilder(
      future: FoodPageModel.getAccountData(_chosenChildId),
      builder: (BuildContext context, AsyncSnapshot snapshot) {
        if (snapshot.connectionState == ConnectionState.done && snapshot.hasData) {
          return WalletPage(model: snapshot.data);
        } else {
          return FoodPagePlaceholder();
        }
      },
    );
  }
}

class WalletPage extends StatefulWidget {
  final FoodPageModel model;

  WalletPage({this.model});

  @override
  _WalletPageState createState() => _WalletPageState();
}

class _WalletPageState extends State<WalletPage> {
  DateFormat _onlyDate = DateFormat('dd.MM.yyyy');
  bool isShowingStats = false;
  List<BalanceSum> balanceSums;

  @override
  void initState() {
    balanceSums = countBalanceSums();
    super.initState();
  }

  List<BalanceSum> countBalanceSums() {
    List<BalanceSum> _balanceSums = widget.model.balances.map((balance) => BalanceSum(keyword: balance.name, isSelected: true, amount: 0)).toList();
    widget.model.transactions.where((transaction) => !transaction.isIncome).forEach((transaction) {
      _balanceSums.firstWhere((balance) => balance.keyword == transaction.keyword).amount += transaction.amount;
    });
    return _balanceSums;
  }

  List<Widget> balanceCards() {
    List<Widget> _list = [SizedBox(width: 15)];
    widget.model.balances.forEach((balanceModel) {
      BalanceConstant _thisBalanceConstant = balanceConstants[balanceModel.name];
      _list.add(
        BalanceCard(
          title: _thisBalanceConstant.ruName,
          icon: _thisBalanceConstant.icon,
          color: _thisBalanceConstant.color,
          amount: balanceModel.balance,
          isSelected: false,
          isPlaceholder: false,
          onTap: () {
            balanceSums.forEach((balanceSum) {
              balanceSum.isSelected = balanceModel.name == balanceSum.keyword;
            });
            showStats();
          },
        ),
      );
    });
    _list.add(SizedBox(width: 15));
    return _list;
  }

  void showStats() {
    setState(() {
      isShowingStats = true;
    });
  }

  void hideStats() {
    setState(() {
      isShowingStats = false;
      balanceSums.forEach((balanceSum) {
        balanceSum.isSelected = true;
      });
    });
  }

  Map<String, double> getPieChartData() {
    Map<String, double> _data = {};
    balanceSums.where((element) => element.isSelected).forEach((element) {
      _data.addAll({element.keyword: element.amount});
    });
    return _data;
  }

  @override
  Widget build(BuildContext context) {
    String _prevDate = '';
    List<TransactionModel> filteredTransactions =
        widget.model.transactions.where((tran) => balanceSums.where((item) => item.isSelected && item.keyword == tran.keyword).length == 1 && (isShowingStats ? !tran.isIncome : true)).toList();
    double _sum = widget.model.transactions.fold(0, (sum, tr) => sum + (tr.isIncome ? 0 : tr.amount));
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Счета'),
            pinned: true,
            floating: true,
            snap: true,
            expandedHeight: 340,
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.none,
              background: Stack(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: Container()),
                        Container(
                          height: 120,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: balanceCards(),
                          ),
                        ),
                        SizedBox(height: 15),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Container(
                                  color: Color(0x20FFFFFF),
                                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child: Material(
                                                  color: Colors.transparent,
                                                  child: InkWell(
                                                    child: Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            'Февраль',
                                                            style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight: FontWeight.w500,
                                                              color: Colors.white,
                                                            ),
                                                          ),
                                                          Icon(
                                                            Icons.arrow_drop_down,
                                                            color: Colors.white,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                    onTap: () {
                                                      print('Открой датапикер');
                                                    },
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(child: Container()),
                                            Container(
                                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                              child: Text(
                                                '$_sum ₽', // TODO: запили сюда сумму
                                                style: TextStyle(
                                                  fontSize: 16,
                                                  fontWeight: FontWeight.w500,
                                                  color: Colors.white,
                                                ),
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 6),
                                      Container(
                                        height: 8,
                                        margin: EdgeInsets.symmetric(horizontal: 6),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(8),
                                          child: Container(
                                            color: Color(0x40FFFFFF),
                                            width: MediaQuery.of(context).size.width,
                                            child: Row(
                                              children: [
                                                ...widget.model.balances.map((item) {
                                                  List<TransactionModel> _thisKeywordTransactions = widget.model.transactions.where((tr) => tr.keyword == item.name).toList();
                                                  return Container(
                                                    width: _thisKeywordTransactions.length > 0 ? (_thisKeywordTransactions.fold(0, (sum, tr) => sum + (tr.isIncome ? 0 : tr.amount)) /
                                                        _sum *
                                                        (MediaQuery.of(context).size.width - 68)) : 0,
                                                    color: balanceConstants[item.name].color,
                                                  );
                                                }),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 6),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  showStats();
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Container(
                                  color: Color(0x20FFFFFF),
                                  padding: EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                      SizedBox(width: 3),
                                      Text(
                                        'Пополнить',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                                onTap: () {
                                  print('Го пополнять');
                                },
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    height: 300,
                    width: MediaQuery.of(context).size.width,
                    child: isShowingStats
                        ? Container(
                            color: primaryColor,
                            child: Column(
                              children: [
                                SizedBox(height: 10),
                                Container(
                                  child: Row(
                                    children: [
                                      SizedBox(width: 15),
                                      Text(
                                        'Траты за ',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                        ),
                                      ),
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(10),
                                        child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            child: Container(
                                              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                              child: Row(
                                                children: [
                                                  Text(
                                                    'Февраль',
                                                    style: TextStyle(
                                                      color: Colors.white,
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  ),
                                                  Icon(
                                                    Icons.arrow_drop_down,
                                                    color: Colors.white,
                                                  ),
                                                ],
                                              ),
                                            ),
                                            onTap: () {
                                              print('Открой датапикер');
                                            },
                                          ),
                                        ),
                                      ),
                                      Expanded(child: Container()),
                                      ClipRRect(
                                        borderRadius: BorderRadius.circular(30),
                                        child: Material(
                                          color: Colors.transparent,
                                          child: InkWell(
                                            child: Container(
                                              padding: EdgeInsets.all(10),
                                              child: Icon(
                                                Icons.close,
                                                size: 30,
                                                color: Colors.white,
                                              ),
                                            ),
                                            onTap: () {
                                              hideStats();
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 10),
                                Container(
                                  height: 34,
                                  child: ListView(
                                    scrollDirection: Axis.horizontal,
                                    children: [
                                      SizedBox(width: 15),
                                      ...balanceSums.map(
                                        (balanceSum) => Padding(
                                          padding: EdgeInsets.only(right: 5),
                                          child: FilterChip(
                                            label: Text(
                                              '${balanceConstants[balanceSum.keyword].ruName} ${balanceSum.amount} ₽',
                                            ),
                                            labelStyle: TextStyle(
                                              color: balanceSum.isSelected ? Colors.white : Colors.black,
                                            ),
                                            selectedColor: balanceConstants[balanceSum.keyword].color,
                                            backgroundColor: Theme.of(context).backgroundColor,
                                            selected: balanceSum.isSelected,
                                            checkmarkColor: Colors.white,
                                            onSelected: (bool isSelected) {
                                              setState(() {
                                                balanceSum.isSelected = isSelected;
                                              });
                                            },
                                          ),
                                        ),
                                      ),
                                      SizedBox(width: 10),
                                    ],
                                  ),
                                ),
                                SizedBox(height: 20),
                                (() {
                                  if (balanceSums.where((item) => item.isSelected && item.amount > 0).length >= 1) {
                                    return PieChart(
                                      dataMap: getPieChartData(),
                                      animationDuration: Duration(milliseconds: 500),
                                      chartRadius: 140,
                                      chartType: ChartType.ring,
                                      colorList: balanceSums.where((item) => item.isSelected).map((item) => balanceConstants[item.keyword].color).toList(),
                                      ringStrokeWidth: 30,
                                      legendOptions: LegendOptions(
                                        showLegends: false,
                                        legendTextStyle: TextStyle(
                                          fontSize: 38,
                                        ),
                                      ),
                                      chartValuesOptions: ChartValuesOptions(
                                        showChartValues: false,
                                      ),
                                    );
                                  } else if (balanceSums.where((item) => item.isSelected).length == 1) {
                                    return Container(
                                      height: 140,
                                    );
                                  } else if (widget.model.transactions.length == 0) {
                                    return Container(
                                      height: 140,
                                    );
                                  } else {
                                    return Container(
                                      height: 140,
                                      child: Center(
                                        child: Text(
                                          'Выберите категорию',
                                          style: TextStyle(color: Colors.white, fontSize: 16),
                                        ),
                                      ),
                                    );
                                  }
                                }()),
                              ],
                            ),
                          )
                        : Container(),
                  ),
                  Positioned(
                    bottom: 90,
                    left: 0,
                    width: MediaQuery.of(context).size.width,
                    child: isShowingStats && balanceSums.where((item) => item.isSelected).length > 0 && widget.model.transactions.length > 0
                        ? Text(
                            '${balanceSums.where((balanceSum) => balanceSum.isSelected).toList().fold(0, (p, c) => p + c.amount)} ₽',
                            textAlign: TextAlign.center,
                            style: TextStyle(
                              fontSize: 18,
                              color: Colors.white,
                              fontWeight: FontWeight.w500,
                            ),
                          )
                        : Container(),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) {
                if (widget.model.transactions.length == 0) {
                  return Container(
                    height: MediaQuery.of(context).size.height - 420,
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: [
                        Container(
                          height: 120,
                          child: Image.asset('assets/wallet.png'),
                        ),
                        SizedBox(height: 10),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text('Нет совершённых операций за этот период',
                            style: TextStyle(
                              fontSize: 20,
                              fontWeight: FontWeight.w600,
                              color: Colors.black54,
                            ),
                            textAlign: TextAlign.center,
                          ),
                        ),
                      ],
                    ),
                  );
                }
                if (index == filteredTransactions.length) {
                  return Container(height: 50);
                }
                TransactionModel _transaction = filteredTransactions[index];
                bool _needDate = false;
                String _thisDate = _onlyDate.format(_transaction.dateTime);
                if (_thisDate != _prevDate) {
                  _needDate = true;
                }
                _prevDate = _thisDate;
                return Container(
                  child: Transaction(
                    transactionModel: _transaction,
                    needDate: _needDate,
                  ),
                );
              },
              childCount: filteredTransactions.length + 1,
            ),
          ),
        ],
      ),
    );
  }
}

class BalanceHelperClass {
  String name;
  double amount;

  BalanceHelperClass({this.name, this.amount});
}

class FoodPagePlaceholder extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: CustomScrollView(
        slivers: [
          SliverAppBar(
            title: Text('Счета'),
            pinned: true,
            floating: true,
            snap: true,
            expandedHeight: 340,
            flexibleSpace: FlexibleSpaceBar(
              collapseMode: CollapseMode.none,
              background: Stack(
                children: [
                  Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Expanded(child: Container()),
                        Container(
                          height: 120,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: [
                              SizedBox(width: 15),
                              ...['Dining', 'Buffet', 'Alga'].map(
                                (keyword) => BalanceCard(
                                  title: balanceConstants[keyword].ruName,
                                  icon: balanceConstants[keyword].icon,
                                  color: balanceConstants[keyword].color,
                                  isSelected: false,
                                  isPlaceholder: true,
                                ),
                              ),
                              SizedBox(width: 15),
                            ],
                          ),
                        ),
                        SizedBox(height: 15),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Container(
                                  color: Color(0x20FFFFFF),
                                  padding: EdgeInsets.symmetric(horizontal: 8, vertical: 6),
                                  child: Column(
                                    children: [
                                      Container(
                                        child: Row(
                                          children: [
                                            Container(
                                              child: ClipRRect(
                                                borderRadius: BorderRadius.circular(10),
                                                child: Material(
                                                  color: Colors.transparent,
                                                  child: InkWell(
                                                    child: Container(
                                                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                                                      child: Row(
                                                        children: [
                                                          Text(
                                                            'Февраль',
                                                            style: TextStyle(
                                                              fontSize: 16,
                                                              fontWeight: FontWeight.w500,
                                                              color: Colors.white,
                                                            ),
                                                          ),
                                                          Icon(
                                                            Icons.arrow_drop_down,
                                                            color: Colors.white,
                                                          ),
                                                        ],
                                                      ),
                                                    ),
                                                  ),
                                                ),
                                              ),
                                            ),
                                            Expanded(child: Container()),
                                            Container(
                                              padding: EdgeInsets.symmetric(horizontal: 15, vertical: 5),
                                              child: PlaceholderWidget(
                                                width: 70,
                                                height: 16,
                                              ),
                                            ),
                                          ],
                                        ),
                                      ),
                                      SizedBox(height: 6),
                                      Container(
                                        height: 8,
                                        margin: EdgeInsets.symmetric(horizontal: 6),
                                        child: ClipRRect(
                                          borderRadius: BorderRadius.circular(8),
                                          child: Container(
                                            color: Color(0x40FFFFFF),
                                            width: MediaQuery.of(context).size.width,
                                            child: Row(
                                              children: [
                                                // AnimatedContainer(
                                                //   width: transactionsSums['Dining'] > 0 ? (transactionsSums['Dining'] / transactionSum * (MediaQuery.of(context).size.width - 68)) : 0,
                                                //   duration: Duration(milliseconds: 250),
                                                //   color: accountColors[0],
                                                // ),
                                                // AnimatedContainer(
                                                //   width: transactionsSums['Buffet'] > 0 ? (transactionsSums['Buffet'] / transactionSum * (MediaQuery.of(context).size.width - 68)) : 0,
                                                //   duration: Duration(milliseconds: 250),
                                                //   color: accountColors[1],
                                                // ),
                                                // AnimatedContainer(
                                                //   width: transactionsSums['Alga'] > 0 ? (transactionsSums['Alga'] / transactionSum * (MediaQuery.of(context).size.width - 68)) : 0,
                                                //   duration: Duration(milliseconds: 250),
                                                //   color: accountColors[2],
                                                // ),
                                              ],
                                            ),
                                          ),
                                        ),
                                      ),
                                      SizedBox(height: 6),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 15),
                        Padding(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10),
                            child: Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Container(
                                  color: Color(0x20FFFFFF),
                                  padding: EdgeInsets.symmetric(vertical: 7),
                                  child: Row(
                                    crossAxisAlignment: CrossAxisAlignment.center,
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    children: [
                                      Icon(
                                        Icons.add,
                                        color: Colors.white,
                                      ),
                                      SizedBox(width: 3),
                                      Text(
                                        'Пополнить',
                                        style: TextStyle(
                                          color: Colors.white,
                                          fontSize: 16,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              ),
                            ),
                          ),
                        ),
                        SizedBox(height: 20),
                      ],
                    ),
                  ),
                ],
              ),
            ),
          ),
          SliverList(
            delegate: SliverChildBuilderDelegate(
              (BuildContext context, int index) => Container(
                height: MediaQuery.of(context).size.height - 420,
                child: LoadingSpinner(),
              ),
              childCount: 1,
            ),
          ),
        ],
      ),
    );
  }
}

class BalanceCard extends StatelessWidget {
  final String title;
  final double amount;
  final Color color;
  final IconData icon;
  final Function onTap;
  final bool isPlaceholder, isSelected;

  BalanceCard({
    this.title,
    this.amount,
    this.color,
    this.icon,
    this.onTap,
    this.isPlaceholder = false,
    this.isSelected = false,
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.symmetric(horizontal: 5),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(10),
        child: Material(
          color: color,
          child: InkWell(
            child: Container(
              width: 150,
              child: Stack(
                children: [
                  Positioned(
                    top: 10,
                    right: 10,
                    width: 60,
                    height: 60,
                    child: Opacity(
                      opacity: 0.3,
                      child: Icon(
                        icon,
                        size: 60,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Positioned(
                    bottom: 0,
                    left: 0,
                    height: 120,
                    width: 150,
                    child: Container(
                      padding: EdgeInsets.symmetric(horizontal: 10, vertical: 9),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(child: Container()),
                          Text(
                            '$title',
                            style: TextStyle(
                              color: Colors.white70.withOpacity(0.8),
                              fontSize: 14,
                            ),
                          ),
                          SizedBox(height: 8),
                          isPlaceholder
                              ? PlaceholderWidget(height: 23, width: 100)
                              : Text(
                                  '${amount.toStringAsFixed(2)} ₽',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                    fontWeight: FontWeight.w500,
                                  ),
                                ),
                        ],
                      ),
                    ),
                  ),
                ],
              ),
            ),
            onTap: onTap,
          ),
        ),
      ),
    );
  }
}

class Transaction extends StatelessWidget {
  final TransactionModel transactionModel;
  final bool needDate;

  Transaction({this.transactionModel, this.needDate = false});

  final DateFormat dayMonth = DateFormat('dd MMMM', 'ru_RU');

  @override
  Widget build(BuildContext context) {
    String _date = '';

    if (needDate) {
      DateTime _today = DateTime.now();
      _today = DateTime(_today.year, _today.month, _today.day);
      if (transactionModel.dateTime.isAfter(DateTime(_today.year, _today.month, _today.day))) {
        _date = 'Сегодня';
      } else if (transactionModel.dateTime.isAfter(DateTime(_today.year, _today.month, _today.day - 1))) {
        _date = 'Вчера';
      } else {
        _date = dayMonth.format(transactionModel.dateTime);
      }
    }
    IconData iconData = balanceConstants[transactionModel.keyword].icon;
    Color iconColor = balanceConstants[transactionModel.keyword].color;

    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Visibility(
          visible: needDate,
          child: Padding(
            padding: EdgeInsets.only(top: 20, bottom: 5, left: 25),
            child: Text(
              _date,
              style: TextStyle(
                fontWeight: FontWeight.w600,
                fontSize: 15,
                color: Colors.black54,
              ),
            ),
          ),
        ),
        InkWell(
          child: Container(
            height: 64,
            padding: EdgeInsets.only(left: 25, right: 25, top: 10, bottom: 10),
            child: Row(
              children: [
                Icon(
                  iconData,
                  size: 35,
                  color: iconColor,
                ),
                SizedBox(width: 15),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      Text(
                        transactionModel.title,
                        style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.w800,
                        ),
                      ),
                      SizedBox(height: 5),
                      Text(transactionModel.isIncome ? 'Пополнение' : 'Оплата'),
                    ],
                  ),
                ),
                Expanded(child: Container()),
                Container(
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.end,
                    children: [
                      Text(
                        (transactionModel.isIncome ? '+ ' : '- ') + '${transactionModel.amount.toStringAsFixed(2)} ₽',
                        style: TextStyle(
                          fontSize: 16,
                          color: transactionModel.isIncome ? Color(0xFF00550C) : Colors.black,
                        ),
                      ),
                      Visibility(
                        visible: transactionModel.compensation > 0,
                        child: SizedBox(height: 5),
                      ),
                      Visibility(
                        visible: transactionModel.compensation > 0,
                        child: Container(
                          padding: EdgeInsets.symmetric(horizontal: 7, vertical: 2),
                          decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(10),
                            color: Colors.green.withOpacity(0.15),
                            border: Border.all(
                              width: 1,
                              color: Colors.green,
                            ),
                          ),
                          child: Text(
                            'Субсидия ${transactionModel.compensation} ₽',
                            style: TextStyle(
                              color: Color(0xFF00550C),
                              fontSize: 12,
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          onTap: () {
            Navigator.push(
              context,
              MaterialPageRoute(
                builder: (context) => TransactionPage(
                  transactionModel: transactionModel,
                  icon: iconData,
                  color: iconColor,
                ),
              ),
            );
          },
        ),
      ],
    );
  }
}

class AccountFilterChip extends StatelessWidget {
  final Color color;
  final String text;
  final Function onTap;
  final bool isSelected;

  AccountFilterChip({this.text, this.color, this.onTap, this.isSelected = false});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(right: 5),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(16),
        child: Material(
          color: color,
          child: InkWell(
            onTap: onTap,
            child: Container(
              padding: EdgeInsets.only(right: 8, left: 12),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: [
                  isSelected
                      ? Container(
                          padding: EdgeInsets.only(right: 5),
                          child: Icon(
                            Icons.check,
                            color: Colors.white,
                            size: 20,
                          ),
                        )
                      : Container(),
                  Text(
                    '$text',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  ),
                  SizedBox(width: 6),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
