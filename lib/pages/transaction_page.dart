import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:elschool_pupil/models/wallet_model.dart';

class TransactionPage extends StatefulWidget {
  final TransactionModel transactionModel;
  final IconData icon;
  final Color color;

  TransactionPage({this.transactionModel, this.icon, this.color});

  @override
  _TransactionPageState createState() => _TransactionPageState();
}

class _TransactionPageState extends State<TransactionPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: widget.color,
        elevation: 0,
      ),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Icon(
                    widget.icon,
                    color: widget.color,
                    size: 60,
                  ),
                  SizedBox(width: 40),
                  Text(
                    '${widget.transactionModel.title}',
                    style: TextStyle(
                      fontSize: 30,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Text(
                '${DateFormat('HH:mm   dd.MM.yyyy', 'ru-RU').format(widget.transactionModel.dateTime)}',
                style: TextStyle(
                  color: Colors.black54,
                  fontSize: 16,
                ),
              ),
            ),
            SizedBox(height: 30),
            Container(
              padding: EdgeInsets.symmetric(horizontal: 20),
              child: Row(
                children: [
                  Expanded(
                    child: Text(
                      '${widget.transactionModel.isIncome ? 'Пополнение' : 'Оплата'}',
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                  Text(
                    '${widget.transactionModel.amount.toStringAsFixed(2)} ₽',
                    style: TextStyle(
                      fontSize: 20,
                      color: widget.transactionModel.isIncome ? Color(0xFF00550C) : Colors.black,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height: 30),
            Visibility(
              visible: widget.transactionModel.compensation > 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                width: MediaQuery.of(context).size.width,
                child: CustomPaint(
                  painter: DashedLinePainter(),
                ),
              ),
            ),
            Visibility(
              visible: widget.transactionModel.compensation > 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                margin: EdgeInsets.only(bottom: 30, top: 30),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Сумма платежа',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text(
                      '${widget.transactionModel.fullCost.toStringAsFixed(2)} ₽',
                      style: TextStyle(
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
            Visibility(
              visible: widget.transactionModel.compensation > 0,
              child: Container(
                padding: EdgeInsets.symmetric(horizontal: 20),
                child: Row(
                  children: [
                    Expanded(
                      child: Text(
                        'Субсидия',
                        style: TextStyle(
                          fontSize: 16,
                        ),
                      ),
                    ),
                    Text(
                      '${widget.transactionModel.compensation.toStringAsFixed(2)} ₽',
                      style: TextStyle(
                        color: Color(0xFF00550C),
                        fontSize: 16,
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class DashedLinePainter extends CustomPainter {
  @override
  void paint(Canvas canvas, Size size) {
    double dashWidth = 9, dashSpace = 5, startX = 0;
    final paint = Paint()
      ..color = Colors.grey
      ..strokeWidth = 1;
    while (startX < size.width) {
      canvas.drawLine(Offset(startX, 0), Offset(startX + dashWidth, 0), paint);
      startX += dashWidth + dashSpace;
    }
  }

  @override
  bool shouldRepaint(CustomPainter oldDelegate) => false;
}
