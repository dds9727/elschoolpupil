import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'dart:ui';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/pages/department_page.dart';
import 'package:elschool_pupil/pages/diary_page.dart';
import 'package:elschool_pupil/pages/grades_n_absences_page.dart';
import 'package:elschool_pupil/pages/grades_page.dart';
import 'package:elschool_pupil/pages/results_page.dart';
import 'package:elschool_pupil/pages/school_page.dart';
import 'package:elschool_pupil/pages/stats_page.dart';

class EducationPage extends StatelessWidget {
  final List<DiaryPageObject> pages = [
    DiaryPageObject(title: 'Табель успеваемости', keyword: 'grades', icon: Icons.table_view, color: Color(0xFFFFB803), page: GradesPage()),
    DiaryPageObject(title: 'Табель и посещаемость', keyword: 'gradesandabsences', icon: Icons.table_view, color: Color(0xFFF95050), page: GradesAndAbsences()),
    DiaryPageObject(title: 'Итоговые', keyword: 'results', icon: Icons.toc, color: Color(0xFF15BB61), page: ResultsPage()),
  ];

  @override
  Widget build(BuildContext context) {
    var chosenChild = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      body: SafeArea(
        child: CustomScrollView(
          slivers: [
            SliverAppBar(
              pinned: false,
              snap: true,
              floating: true,
              backgroundColor: Theme.of(context).backgroundColor,
              title: Text(
                'Обучение',
                style: TextStyle(
                  color: Colors.black,
                ),
              ),
            ),
            SliverList(
              delegate: SliverChildListDelegate(
                [
                  SizedBox(height: 20),
                  Center(
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(100),
                      child: Container(
                        color: Colors.white,
                        height: 100,
                        width: 100,
                        child: Image.network(
                          '${chosenChild.avatarUrl}',
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Container(
                    width: MediaQuery.of(context).size.width,
                    child: Text(
                      '${chosenChild.name}',
                      style: TextStyle(
                        fontSize: 18,
                        fontWeight: FontWeight.w500,
                      ),
                      softWrap: true,
                      textAlign: TextAlign.center,
                    ),
                  ),
                  SizedBox(height: 20),
                  ...chosenChild.departments.map(
                    (department) => MaterialRow(
                      title: '${department.name}',
                      subtitle: 'Класс',
                      icon: Icons.people,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => DepartmentPage(departmentId: 3228),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 15),
                  ...chosenChild.schools.map(
                    (school) => MaterialRow(
                      title: '${school.name}',
                      subtitle: 'Образовательное учреждение',
                      icon: Icons.apartment_outlined,
                      onTap: () {
                        Navigator.push(
                          context,
                          MaterialPageRoute(
                            builder: (context) => SchoolPage(schoolId: 3229),
                          ),
                        );
                      },
                    ),
                  ),
                  SizedBox(height: 5),
                  Container(
                    height: 300,
                    child: GridView.count(
                      primary: false,
                      crossAxisCount: 3,
                      mainAxisSpacing: 10,
                      crossAxisSpacing: 10,
                      childAspectRatio: 1.0,
                      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 20),
                      children: [
                        DiaryPageBlock(
                          item: DiaryPageObject(
                            title: 'Дневник',
                            color: Color(0xFF4D70FF),
                            icon: Icons.import_contacts,
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => DiaryPage(),
                              ),
                            );
                          },
                        ),
                        ...pages.map(
                          (page) => DiaryPageBlock(
                            item: page,
                            onTap: () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => page.page,
                                ),
                              );
                            },
                          ),
                        ),
                        DiaryPageBlock(
                          item: DiaryPageObject(
                            title: 'Статистика',
                            color: Color(0xFF6f33ff),
                            icon: Icons.bar_chart,
                          ),
                          onTap: () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                builder: (context) => StatsPage(),
                              ),
                            );
                          },
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 50),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MaterialRow extends StatelessWidget {
  final IconData icon;
  final String title, subtitle;
  final Function onTap;

  MaterialRow({this.title, this.subtitle, this.icon, this.onTap});

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20, vertical: 5),
            child: Row(
              children: [
                SizedBox(width: 0),
                Icon(
                  icon,
                  size: 42,
                  color: primaryColor,
                ),
                SizedBox(width: 18),
                Expanded(
                  child: Container(
                    child: Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Text('$title'.replaceAll("", "\u{200B}"),
                          style: TextStyle(
                            fontSize: 16,
                          ),
                          softWrap: false,
                          overflow: TextOverflow.ellipsis,
                        ),
                        Text('$subtitle'.replaceAll("", "\u{200B}"),
                          style: TextStyle(
                            fontSize: 14,
                            color: Color(0xFF888888),
                          ),
                          softWrap: false,
                          overflow: TextOverflow.ellipsis,
                        ),
                      ],
                    ),
                  ),
                ),
                SizedBox(width: 20),
                Icon(
                  Icons.arrow_forward,
                  size: 24,
                  color: primaryColor,
                ),
              ],
            ),
          ),
          onTap: onTap,
      ),
    );
  }
}

class DiaryPageBlock extends StatelessWidget {
  final Function onTap;
  final DiaryPageObject item;

  DiaryPageBlock({this.item, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          splashColor: item.color.withOpacity(0.05),
          highlightColor: item.color.withOpacity(0.05),
          onTap: onTap,
          child: Container(
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                SizedBox(height: 10),
                Container(
                  width: 60,
                  height: 60,
                  padding: EdgeInsets.all(10),
                  color: item.color.withOpacity(0.1),
                  child: Icon(
                    item.icon,
                    color: item.color,
                    size: 40,
                  ),
                ),
                SizedBox(height: 10),
                Text(
                  '${item.title}',
                  style: TextStyle(
                    color: Colors.black.withOpacity(0.65),
                    fontSize: 14,
                    fontWeight: FontWeight.w600,
                  ),
                  textAlign: TextAlign.center,
                  overflow: TextOverflow.ellipsis,
                  maxLines: 2,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class DiaryPageObject {
  String title, keyword;
  IconData icon;
  Color color;
  Widget page;

  DiaryPageObject({this.title, this.keyword, this.icon, this.color, this.page});
}
