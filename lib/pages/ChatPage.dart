import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'dart:convert';
import 'package:http/http.dart' as http;
import 'package:elschool_pupil/helpers/loading_spinner.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/messenger_models.dart';

class ChatPage extends StatefulWidget {
  final int chatId;
  final MessengerContact messengerContact;

  ChatPage({this.chatId, this.messengerContact});

  @override
  _ChatPageState createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  bool isLoading = false;
  ScrollController _scrollController;
  TextEditingController inputController = TextEditingController();

  @override
  void initState() {
    _scrollController = ScrollController();
    super.initState();
    if (widget.messengerContact.messages.length <= 1) {
      getMessages();
    }
  }

  void getMessages() async {
    setState(() {
      isLoading = true;
    });
    var response = await http.get(Uri.https('api.mocki.io', '/v1/6b6cf870'));
    List<ChatMessage> _messages = (json.decode(response.body) as List).map((i) => ChatMessage.fromJson(i)).toList();
    await new Future.delayed(const Duration(milliseconds: 1000));
    if (mounted) setState(() {
      widget.messengerContact.messages.addAll(_messages);
      isLoading = false;
      _scrollController.jumpTo(_scrollController.position.maxScrollExtent);
    });
  }

  @override
  Widget build(BuildContext context) {
    var userId = context.select<MainUserInfo, int>((userInfo) => userInfo.id);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        backgroundColor: Theme.of(context).backgroundColor,
        iconTheme: IconThemeData(
          color: primaryColor,
        ),
        actions: [
          IconButton(
            icon: Icon(
              Icons.more_vert_outlined,
              size: 28,
            ),
            onPressed: () {},
          ),
        ],
        title: Container(
          child: Row(
            children: [
              ClipRRect(
                borderRadius: BorderRadius.circular(100),
                child: Container(
                  color: Colors.white,
                  height: 44,
                  width: 44,
                  child: Image.network(
                    widget.messengerContact.pictureUrl,
                    fit: BoxFit.cover,
                  ),
                ),
              ),
              SizedBox(width: 10),
              Expanded(
                child: Text(
                  '${widget.messengerContact.name}'.replaceAll("", "\u{200B}"),
                  style: TextStyle(
                    color: Colors.black,
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      body: Column(
        children: [
          Expanded(
            child: Container(
              width: MediaQuery.of(context).size.width,
              child: ScrollConfiguration(
                behavior: NoGlowBehaviour(),
                child: Container(
                  constraints: BoxConstraints(minHeight: MediaQuery.of(context).size.height - 130),
                  child: ListView.builder(
                    controller: _scrollController,
                    padding: EdgeInsets.only(top: 30, bottom: 10),
                    reverse: true,
                    itemBuilder: (BuildContext buildContext, int index) {
                      if (isLoading && index == widget.messengerContact.messages.length) {
                        return Center(
                          child: LoadingSpinner(),
                        );
                      }
                      return ChatMessageBubble(
                        message: widget.messengerContact.messages[index],
                        userId: userId,
                      );
                    },
                    itemCount: widget.messengerContact.messages.length + (isLoading ? 1 : 0),
                  ),
                ),
              ),
            ),
          ),
          Container(
            height: 50,
            decoration: BoxDecoration(
              border: Border(
                top: BorderSide(
                  width: 1,
                  color: Color(0xFFDDDDDD),
                ),
              ),
            ),
            child: Row(
              children: [
                IconButton(
                  icon: Icon(Icons.attach_file_outlined),
                  color: Color(0xFF888888),
                  onPressed: () {},
                ),
                Expanded(
                  child: Theme(
                    data: ThemeData(
                      primaryColor: primaryColor,
                    ),
                    child: TextField(
                      controller: inputController,
                      cursorHeight: 24,
                      decoration: InputDecoration(
                        border: OutlineInputBorder(
                          borderSide: BorderSide.none,
                        ),
                        filled: false,
                        contentPadding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                      ),
                      style: TextStyle(
                        fontSize: 20,
                      ),
                    ),
                  ),
                ),
                IconButton(
                  icon: Icon(Icons.send),
                  color: Color(0xFF888888),
                  onPressed: () {
                    ChatMessage newMessage = ChatMessage(
                      dateTime: DateTime.now(),
                      senderId: userId,
                      text: inputController.text,
                      messageStatus: 'sent',
                    );
                    setState(() {
                      widget.messengerContact.messages.add(newMessage);
                    });
                    inputController.clear();
                  },
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class ChatMessageBubble extends StatelessWidget {
  final ChatMessage message;
  final int userId;

  ChatMessageBubble({this.message, this.userId});

  @override
  Widget build(BuildContext context) {
    bool _isFromThisUser = message.senderId == userId;
    bool _isMessageRead = message.messageStatus == 'read';
    return Container(
      margin: EdgeInsets.only(
        right: _isFromThisUser ? 10 : (MediaQuery.of(context).size.width * 0.3 + 10),
        left: _isFromThisUser ? (MediaQuery.of(context).size.width * 0.3 + 10) : 10,
      ),
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: _isFromThisUser ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.symmetric(vertical: 5),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: _isFromThisUser ? Color(0xFFB8E8FF) : Color(0xFFEDEDED),
            ),
            child: Stack(
              children: [
                Container(
                  padding: EdgeInsets.symmetric(vertical: 10, horizontal: 15),
                  child: RichText(
                    text: TextSpan(
                      text: message.text,
                      style: TextStyle(
                        fontSize: 14,
                        color: Colors.black,
                      ),
                      children: [
                        TextSpan(
                          text: ' 22:43' + (_isFromThisUser ? ' ^^' : '') + (_isFromThisUser && _isMessageRead ? '^^' : ''),
                          style: TextStyle(
                            fontSize: 12,
                            color: Colors.transparent,
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
                Positioned(
                  bottom: 4,
                  right: 12,
                  child: Container(
                    child: Row(
                      children: [
                        Text(
                          DateFormat('HH:mm', 'ru-RU').format(message.dateTime),
                          style: TextStyle(
                            fontSize: 12,
                            color: Color(0xFF666666),
                          ),
                        ),
                        SizedBox(width: _isFromThisUser ? 4 : 0),
                        _isFromThisUser
                            ? Container(
                                width: _isMessageRead ? 18 : 12,
                                height: 16,
                                child: Stack(
                                  children: [
                                    Positioned(
                                      top: 1,
                                      left: 0,
                                      child: FaIcon(
                                        FontAwesomeIcons.check,
                                        color: primaryColor,
                                        size: 12,
                                      ),
                                    ),
                                    _isMessageRead
                                        ? Positioned(
                                            top: 1,
                                            left: 6,
                                            child: FaIcon(
                                              FontAwesomeIcons.check,
                                              color: primaryColor,
                                              size: 12,
                                            ),
                                          )
                                        : Container(),
                                  ],
                                ),
                              )
                            : Container(),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
