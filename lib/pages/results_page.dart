import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/period_model.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/grades_absences_results_models.dart';
import 'package:elschool_pupil/helpers/choose_year_widget.dart';
import 'package:elschool_pupil/helpers/filled_input.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/choose_child_button.dart';

class ResultsPage extends StatefulWidget {
  final Period period;

  ResultsPage({Key key, this.period}) : super(key: key);

  @override
  _ResultsPageState createState() => _ResultsPageState();
}

class _ResultsPageState extends State<ResultsPage> {
  String searchValue = '';
  List<SubjectResults> subjectResults = [];
  bool isLoading = true;

  Period chosenPeriod = Period(id: 13, years: '2020-2021');
  List<Period> periods = [
    Period(id: 9, years: '2016-2017'),
    Period(id: 10, years: '2017-2018'),
    Period(id: 11, years: '2018-2019'),
    Period(id: 12, years: '2019-2020'),
    Period(id: 13, years: '2020-2021'),
  ];

  @override
  void initState() {
    super.initState();
    getResults(chosenPeriod);
  }

  void getResults(Period _chosenPeriod) async {
    //api.mocki.io/v1/d4e309f3
    int chosenChildId = context.read<MainUserInfo>().chosenChild.id;

    var response = await http.get(Uri.https('api.mocki.io', '/v1/d4e309f3'));
    List<SubjectResults> _subjectResults = (json.decode(response.body) as List).map((i) => SubjectResults.fromJson(i)).toList();
    if (mounted) setState(() {
      subjectResults = _subjectResults;
      isLoading = false;
    });
  }

  Color averageColor(double mark) {
    Color color;
    if (mark == null) return Colors.transparent;
    if (mark >= 4.5) {
      color = Color(0xFF98FB98);
    } else if (mark >= 3.5) {
      color = Color(0xFF87CEEB);
    } else if (mark >= 2.5) {
      color = Color(0xFFF0E68C);
    } else if (mark > 0) {
      color = Color(0xFFFFB6C1);
    } else {
      color = Colors.transparent;
    }
    return color;
  }

  void changePeriod(Period newPeriod) {
    setState(() {
      isLoading = true;
      chosenPeriod = newPeriod;
    });
    getResults(newPeriod);
  }

  @override
  Widget build(BuildContext context) {
    var chosenChild = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text('Итоговые'),
        elevation: 0,
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            color: primaryColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 60,
                  width: constraints.maxWidth,
                  child: Row(
                    children: [
                      SizedBox(width: 15),
                      Expanded(
                        child: ChooseChildButton(
                          onChildChosen: () {
                            setState(() {
                              isLoading = true;
                            });
                            getResults(chosenPeriod);
                          },
                        ),
                      ),
                      SizedBox(width: 10),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.date_range,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    '${chosenPeriod.years}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () async {
                              var result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ChooseYearWidget(periods: periods),
                                ),
                              );
                              if (result != null) {
                                if (result != null) {
                                  changePeriod(periods.firstWhere((period) => period.id == result));
                                }
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                    ],
                  ),
                ),
                Container(
                  height: constraints.maxHeight - 60,
                  width: constraints.maxWidth,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    color: Theme.of(context).backgroundColor,
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 74,
                        width: constraints.maxWidth,
                        padding: EdgeInsets.only(top: 15, right: 25, bottom: 10, left: 25),
                        child: FilledInput(
                          onChanged: (value) {
                            setState(() {
                              searchValue = value;
                            });
                          },
                        ),
                      ),
                      Container(
                        height: 1,
                        color: Colors.black38,
                      ),
                      Container(
                        height: constraints.maxHeight - 135,
                        width: constraints.maxWidth,
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(height: 10),
                              isLoading
                                  ? Container(
                                child: Column(
                                  children: List.generate(3, (index) => PlaceholderResultsCard()),
                                ),
                              )
                                  : Container(),
                              ...subjectResults.map(
                                    (subject) => Visibility(
                                  visible: subject.title.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0,
                                  child: Card(
                                    margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                                      child: Column(
                                        children: [
                                          Text(
                                            '${subject.title}',
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Container(
                                            height: 1,
                                            color: Color(0xFFD1D1D1),
                                          ),
                                          SizedBox(height: 10),
                                          ...subject.resultGrades.map((period) {
                                            return Container(
                                              margin: EdgeInsets.symmetric(vertical: 10),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '${period.periodName}',
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  ),
                                                  SizedBox(width: 10),
                                                  Expanded(child: Container()),
                                                  SizedBox(width: 10),
                                                  Container(
                                                    margin: EdgeInsets.only(top: 3),
                                                    width: 12,
                                                    height: 12,
                                                    decoration: BoxDecoration(
                                                      color: averageColor(period.mark),
                                                      borderRadius: BorderRadius.circular(20),
                                                    ),
                                                  ),
                                                  SizedBox(width: 10),
                                                  Text(
                                                    '${period.mark != null ? period.mark : ''}',
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.w600,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            );
                                          }),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PlaceholderResultsCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: [
            PlaceholderWidget(width: 150, height: 22),
            SizedBox(height: 15),
            Container(
              height: 1,
              color: Color(0xFFD1D1D1),
            ),
            SizedBox(height: 10),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  Expanded(child: Container()),
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: PlaceholderWidget(width: 12, height: 12),
                    ),
                  ),
                  SizedBox(width: 10),
                  PlaceholderWidget(width: 23, height: 20),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  Expanded(child: Container()),
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: PlaceholderWidget(width: 12, height: 12),
                    ),
                  ),
                  SizedBox(width: 10),
                  PlaceholderWidget(width: 23, height: 20),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  Expanded(child: Container()),
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: PlaceholderWidget(width: 12, height: 12),
                    ),
                  ),
                  SizedBox(width: 10),
                  PlaceholderWidget(width: 23, height: 20),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  Expanded(child: Container()),
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: PlaceholderWidget(width: 12, height: 12),
                    ),
                  ),
                  SizedBox(width: 10),
                  PlaceholderWidget(width: 23, height: 20),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 40, height: 20),
                  Expanded(child: Container()),
                  Container(
                    margin: EdgeInsets.only(top: 3),
                    child: ClipRRect(
                      borderRadius: BorderRadius.circular(20),
                      child: PlaceholderWidget(width: 12, height: 12),
                    ),
                  ),
                  SizedBox(width: 10),
                  PlaceholderWidget(width: 23, height: 20),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
