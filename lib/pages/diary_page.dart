import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/rendering.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/diary_model.dart';
import 'package:elschool_pupil/helpers/loading_spinner.dart';
import 'package:elschool_pupil/helpers/choose_child_button.dart';

class DiaryPage extends StatefulWidget {
  @override
  _DiaryPageState createState() => _DiaryPageState();
}

class _DiaryPageState extends State<DiaryPage> {
  List<DaySchedule> daySchedules = [];
  DateTime chosenDate;

  @override
  void initState() {
    DateTime _now = DateTime.now();
    chosenDate = DateTime(_now.year, _now.month, _now.day);
    daySchedules = getWeekByDate(chosenDate);
    super.initState();
    fetchDaySchedule(chosenDate);
  }

  List<DaySchedule> getWeekByDate(DateTime date) {
    List<DaySchedule> _daySchedules = [];
    DateTime firstDay = date.add(Duration(days: (-date.weekday + 1)));
    for (int i = 0; i < 7; i++) {
      _daySchedules.add(
        DaySchedule(
          date: firstDay.add(Duration(days: i)),
          lessons: [],
          isLoading: true,
        ),
      );
    }
    return _daySchedules;
  }

  void fetchDaySchedule(DateTime date) async {
    int chosenChildId = context.read<MainUserInfo>().chosenChild.id;
    String url = '/v1/5ae4a2c0';
    if (date.weekday == 7) {
      url = '/v1/68c0ed3e';
    } else {
      url = '/v1/5ae4a2c0';
    }
    var response = await http.get(Uri.https('api.mocki.io', url));
    List<Lesson> _lessons = (json.decode(response.body) as List).map((i) => Lesson.fromJson(i)).toList();
    DaySchedule _daySchedule = DaySchedule(date: date, lessons: _lessons, isLoading: false);
    if (mounted) setState(() {
      int _index = daySchedules.indexWhere((daySchedule) => daySchedule.date.isAtSameMomentAs(date));
      if (_index >= 0) {
        daySchedules[_index] = _daySchedule;
      }
    });
  }

  void chooseDate(DateTime date) {
    setState(() {
      chosenDate = date;
      if (date.isBefore(daySchedules[0].date) || date.isAfter(daySchedules[6].date)) {
        daySchedules = getWeekByDate(date);
      }
    });
    if (daySchedules.singleWhere((daySchedule) => daySchedule.date.isAtSameMomentAs(date)).isLoading) {
      fetchDaySchedule(date);
    }
  }

  String getMonthOfChosenDate() {
    String _month = '';
    _month = DateFormat('MMMM', 'ru-RU').format(daySchedules[0].date);
    if (daySchedules[0].date.month != daySchedules[6].date.month) {
      _month += ' - ' + DateFormat('MMMM', 'ru-RU').format(daySchedules[6].date);
    }
    return _month[0].toUpperCase() + _month.substring(1);
  }

  @override
  Widget build(BuildContext context) {
    int _index = 0;
    DaySchedule _chosenDay = daySchedules.singleWhere((daySchedule) => daySchedule.date.isAtSameMomentAs(chosenDate));
    List<Lesson> _chosenDayLessons = _chosenDay.lessons;
    ChildInfo chosenChild = context.read<MainUserInfo>().chosenChild;

    return  Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text('Дневник'),
        elevation: 0,
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            color: primaryColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 60,
                  width: constraints.maxWidth,
                  child: Row(
                    children: [
                      SizedBox(width: 15),
                      Expanded(
                        child: ChooseChildButton(
                          onChildChosen: () {
                            setState(() {
                              daySchedules = getWeekByDate(chosenDate);
                            });
                            fetchDaySchedule(chosenDate);
                          },
                        ),
                      ),
                      SizedBox(width: 10),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.date_range,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    '${getMonthOfChosenDate()}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () {
                              showDatePicker(
                                helpText: 'Выберите дату',
                                context: context,
                                initialDate: chosenDate,
                                firstDate: DateTime(
                                    DateTime.now().year - 3),
                                lastDate: DateTime(
                                    DateTime.now().year + 3),
                                fieldLabelText: 'Выберите дату',
                                fieldHintText: 'дд/мм/гггг',
                                //locale: Locale('ru'),
                              ).then((result) {
                                if (result != null) {
                                  chooseDate(result);
                                }
                              });
                            },
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                    ],
                  ),
                ),
                Container(
                  height: constraints.maxHeight - 60,
                  width: constraints.maxWidth,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    color: Theme.of(context).backgroundColor,
                  ),
                  child: Container(
                    child: Column(
                      children: [
                        Container(
                          height: 74,
                          width: constraints.maxWidth,
                          padding: EdgeInsets.only(top: 15, right: 10, bottom: 10, left: 10),
                          child: Row(
                            mainAxisSize: MainAxisSize.max,
                            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: [
                              ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      child: Icon(
                                        Icons.chevron_left,
                                        size: 30,
                                      ),
                                    ),
                                    onTap: () {
                                      chooseDate(daySchedules[0].date.add(Duration(days: -1)));
                                    },
                                  ),
                                ),
                              ),
                              ...daySchedules.map(
                                    (day) => WeekDay(
                                  day: day.date,
                                  isActive: day.date.isAtSameMomentAs(chosenDate),
                                  onTap: () {
                                    chooseDate(day.date);
                                  },
                                ),
                              ),
                              ClipRRect(
                                borderRadius: BorderRadius.circular(50),
                                child: Material(
                                  color: Colors.transparent,
                                  child: InkWell(
                                    child: Container(
                                      padding: EdgeInsets.all(5),
                                      child: Icon(
                                        Icons.chevron_right,
                                        size: 30,
                                      ),
                                    ),
                                    onTap: () {
                                      chooseDate(daySchedules[0].date.add(Duration(days: 7)));
                                      },
                                  ),
                                ),
                              ),
                            ],
                          ),
                        ),
                        Container(
                          height: 1,
                          color: Colors.black38,
                        ),
                        Container(
                          height: constraints.maxHeight - 135,
                          width: constraints.maxWidth,
                          child: SingleChildScrollView(
                            child: Column(
                              children: [
                                SizedBox(height: 10),
                                Visibility(
                                  visible: _chosenDayLessons.length > 0,
                                  child: Container(
                                    child: Row(
                                      children: [
                                        SizedBox(width: 15),
                                        Container(
                                          margin: EdgeInsets.symmetric(horizontal: 10.5),
                                          width: 3,
                                          height: 20,
                                          color: primaryColor,
                                        ),
                                        SizedBox(width: 90),
                                        Text(
                                          'Уроки',
                                          style: TextStyle(
                                            color: Colors.black54,
                                          ),
                                        ),
                                      ],
                                    ),
                                  ),
                                ),
                                _chosenDay.isLoading ? Container(
                                  width: 100,
                                  height: constraints.maxHeight - 145,
                                  child: LoadingSpinner(),
                                ) : Container(),
                                (_chosenDayLessons.length == 0 && !_chosenDay.isLoading)
                                    ? Container(
                                        height: constraints.maxHeight - 145,
                                        child: Center(
                                          child: Column(
                                            mainAxisAlignment: MainAxisAlignment.center,
                                            children: [
                                              Container(
                                                width: constraints.maxWidth - 100,
                                                child: Image.asset(chosenChild.gender == 'Female' ? 'assets/girl_chill.png' : 'assets/boy_chill.png'),
                                              ),
                                              Container(
                                                height: 100,
                                                child: Text(
                                                  'Уроков нет',
                                                  style: TextStyle(
                                                    color: Colors.black54,
                                                    fontSize: 24,
                                                    fontWeight: FontWeight.w600,
                                                  ),
                                                ),
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    : Container(),
                                ..._chosenDayLessons.map((lesson) {
                                  _index++;
                                  return LessonBlock(
                                    lesson: lesson,
                                    width: constraints.maxWidth,
                                    marginBottom: _index == _chosenDayLessons.length ? 50 : 0,
                                  );
                                }),
                              ],
                            ),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class WeekDay extends StatelessWidget {
  final DateTime day;
  final bool isActive;
  final Function onTap;

  WeekDay({this.day, this.isActive = false, this.onTap});

  @override
  Widget build(BuildContext context) {
    return ClipRRect(
      borderRadius: BorderRadius.circular(10),
      child: Material(
        color: Colors.transparent,
        child: InkWell(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
            decoration: BoxDecoration(
              color: isActive ? primaryColor : Colors.transparent,
              borderRadius: BorderRadius.circular(10),
            ),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Text(
                  DateFormat('E', 'ru-RU').format(day).toUpperCase(),
                  style: TextStyle(
                    color: isActive ? Color(0xFFCCCCCC) : Colors.black54,
                    // fontWeight: FontWeight.w600,
                  ),
                ),
                SizedBox(height: 3),
                Text(
                  DateFormat('dd', 'ru-RU').format(day),
                  style: TextStyle(
                    fontSize: 16,
                    color: isActive ? Colors.white : Colors.black,
                    // fontWeight: FontWeight.w600,
                  ),
                ),
              ],
            ),
          ),
          onTap: onTap,
        ),
      ),
    );
  }
}

class LessonBlock extends StatelessWidget {
  final Lesson lesson;
  final double marginBottom, width;

  LessonBlock({this.lesson, this.marginBottom = 0, this.width});

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: marginBottom),
      width: width,
      child: IntrinsicHeight(
        child: Row(
          children: [
            SizedBox(width: 15),
            Container(
              width: 24,
              child: Column(
                children: <Widget>[
                  SizedBox(height: 10),
                  ClipRRect(
                    borderRadius: BorderRadius.all(Radius.circular(20)),
                    child: Container(
                      width: 12,
                      height: 12,
                      color: primaryColor,
                      padding: EdgeInsets.all(2.5),
                      child: ClipRRect(
                        borderRadius: BorderRadius.all(Radius.circular(20)),
                        child: Container(
                          height: 7,
                          width: 7,
                          color: Colors.white,
                          padding: EdgeInsets.all(2),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(height: 10),
                  Expanded(
                    child: Container(
                      margin: EdgeInsets.symmetric(horizontal: 10.5),
                      width: 3,
                      color: primaryColor,
                    ),
                  ),
                ],
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(vertical: 10),
              width: 70,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Text(
                    '${DateFormat('HH:mm', 'ru_RU').format(lesson.start)}',
                    style: TextStyle(
                      fontSize: 16,
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                  Text(
                    '${lesson.number}',
                    style: TextStyle(
                      fontSize: 30,
                      fontWeight: FontWeight.w600,
                    ),
                  ),
                  Text(
                    '${DateFormat('HH:mm', 'ru_RU').format(lesson.end)}',
                    style: TextStyle(
                      fontWeight: FontWeight.w500,
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(width: 10),
            Expanded(
              child: Container(
                margin: EdgeInsets.only(top: 10, bottom: 10),
                decoration: BoxDecoration(
                  color: Color(0xFFE9F0F5),
                  borderRadius: BorderRadius.all(Radius.circular(10)),
                ),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Container(
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Expanded(
                            child: Container(
                              padding: EdgeInsets.only(top: 15, left: 15, right: 15),
                              child: Text(
                                '${lesson.title}',
                                style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w600,
                                ),
                              ),
                            ),
                          ),
                          lesson.mark != null
                              ? Container(
                                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 10),
                                width: 90,
                                decoration: markBoxDecoration(lesson.mark),
                                child: Center(
                                  child: Column(
                                    children: [
                                      Text(
                                        '${lesson.mark}',
                                        style: TextStyle(
                                          fontSize: 20,
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                      SizedBox(height: 3),
                                      Text(
                                        'Оценка',
                                        style: TextStyle(
                                          fontWeight: FontWeight.w500,
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )
                              : Container(),
                        ],
                      ),
                    ),
                    SizedBox(height: 12),
                    Container(
                      padding: EdgeInsets.symmetric(horizontal: 15),
                      child: Row(
                        children: [
                          Expanded(
                            child: Visibility(
                              visible: lesson.number > 0,
                              child: IconNText(
                                text: '${lesson.number} урок',
                                icon: Icons.access_time,
                              ),
                            ),
                          ),
                          Expanded(
                            child: Visibility(
                              visible: lesson.room != null,
                              child: IconNText(
                                text: 'каб. ${lesson.room}',
                                icon: Icons.location_on,
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Visibility(
                      visible: lesson.classwork != null,
                      child: WorkBlock(
                        title: 'Этапы урока:',
                        text: lesson.classwork,
                      ),
                    ),
                    Visibility(
                      visible: lesson.homework != null,
                      child: WorkBlock(
                        title: 'Домашнее задание:',
                        text: lesson.homework,
                      ),
                    ),
                    Visibility(
                      visible: lesson.note != null,
                      child: TeacherComment(note: lesson.note),
                    ),
                    Visibility(
                      visible: lesson.teacherName != null,
                      child: SizedBox(height: 7),
                    ),
                    Visibility(
                      visible: lesson.teacherName != null,
                      child: Container(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.end,
                          children: [
                            Text(
                              '${lesson.teacherName}',
                              style: TextStyle(
                                color: Colors.black54,
                                fontStyle: FontStyle.italic,
                              ),
                            ),
                            SizedBox(width: 7),
                            Material(
                              color: Colors.transparent,
                              child: InkWell(
                                child: Icon(
                                  Icons.message,
                                  size: 18,
                                ),
                                onTap: () {
                                  //do some
                                },
                              ),
                            ),
                          ],
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                  ],
                ),
              ),
            ),
            SizedBox(width: 15),
          ],
        ),
      ),
    );
  }
}

BoxDecoration markBoxDecoration(String mark) {
  String mark2 = '';
  int index = mark.indexOf('/');
  if (index > 0) {
    mark2 = mark.substring(index + 1, mark.length);
    mark = mark.substring(0, index);
  }
  if (mark2 != '') {
    return BoxDecoration(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(10),
        bottomLeft: Radius.circular(10),
      ),
      gradient: LinearGradient(
        begin: Alignment.centerLeft,
        end: Alignment.centerRight,
        colors: [markColor(mark), markColor(mark2)],
      ),
    );
  } else {
    return BoxDecoration(
      borderRadius: BorderRadius.only(
        topRight: Radius.circular(10),
        bottomLeft: Radius.circular(10),
      ),
      color: markColor(mark),
    );
  }
}

Color markColor(String mark) {
  Color color;
  switch (mark) {
    case '5':
      color = Color(0xFF98FB98);
      break;
    case '4':
      color = Color(0xFF87CEEB);
      break;
    case '3':
      color = Color(0xFFF0E68C);
      break;
    case '2':
    case '1':
      color = Color(0xFFFFB6C1);
      break;
    default:
      color = Color(0xFFBBBBBB);
      break;
  }
  return color;
}

class IconNText extends StatelessWidget {
  final String text;
  final IconData icon;

  IconNText({this.icon, this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Row(
        children: [
          Icon(icon, size: 16),
          SizedBox(width: 5),
          Text(text),
        ],
      ),
    );
  }
}

class WorkBlock extends StatelessWidget {
  final String title, text;

  WorkBlock({this.title, this.text});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          Container(
            height: 1,
            color: Colors.black38,
          ),
          SizedBox(height: 15),
          Text(
            title,
            style: TextStyle(
              color: Colors.black54,
              fontStyle: FontStyle.italic,
            ),
          ),
          SizedBox(height: 7),
          Text(
            text,
          )
        ],
      ),
    );
  }
}

class TeacherComment extends StatelessWidget {
  final String note;

  TeacherComment({this.note});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          SizedBox(height: 15),
          Container(
            height: 1,
            color: Colors.black38,
          ),
          SizedBox(height: 15),
          Visibility(
            visible: note != null,
            child: Text('"$note"'),
          ),
        ],
      ),
    );
  }
}