import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';
import 'package:elschool_pupil/models/user_info.dart';
import 'package:elschool_pupil/models/period_model.dart';
import 'package:elschool_pupil/models/constants.dart';
import 'package:elschool_pupil/models/grades_absences_results_models.dart';
import 'package:elschool_pupil/helpers/placeholder.dart';
import 'package:elschool_pupil/helpers/choose_year_widget.dart';
import 'package:elschool_pupil/helpers/filled_input.dart';
import 'package:elschool_pupil/helpers/choose_child_button.dart';

class GradesAndAbsences extends StatefulWidget {
  final Period period;

  GradesAndAbsences({Key key, this.period}) : super(key: key);

  @override
  _GradesAndAbsencesState createState() => _GradesAndAbsencesState();
}

class _GradesAndAbsencesState extends State<GradesAndAbsences> {
  String searchValue = '';

  Period chosenPeriod = Period(id: 13, years: '2020-2021');
  List<Period> periods = [
    Period(id: 9, years: '2016-2017'),
    Period(id: 10, years: '2017-2018'),
    Period(id: 11, years: '2018-2019'),
    Period(id: 12, years: '2019-2020'),
    Period(id: 13, years: '2020-2021'),
  ];

  List<SubjectGradesAndAbsences> subjects = [];

  @override
  void initState() {
    super.initState();
    getGrades(chosenPeriod);
  }

  void getGrades(Period _chosenPeriod) async {
    int chosenChildId = context.read<MainUserInfo>().chosenChild.id;
    var response = await http.get(Uri.https('api.mocki.io', '/v1/df1a3150'));
    List<SubjectGradesAndAbsences> _subjects = (json.decode(response.body) as List).map((i) => SubjectGradesAndAbsences.fromJson(i)).toList();
    if (mounted) setState(() {
      subjects = _subjects;
      isLoading = false;
    });
  }

  bool isLoading = true;

  void changePeriod(Period newPeriod) {
    setState(() {
      isLoading = true;
      chosenPeriod = newPeriod;
    });
    getGrades(newPeriod);
  }

  @override
  Widget build(BuildContext context) {
    var chosenChild = context.select<MainUserInfo, ChildInfo>((userInfo) => userInfo.chosenChild);

    return Scaffold(
      backgroundColor: Theme.of(context).backgroundColor,
      appBar: AppBar(
        title: Text('Табель и посещаемость'),
        elevation: 0,
      ),
      body: SafeArea(
        child: LayoutBuilder(
          builder: (context, constraints) => Container(
            color: primaryColor,
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  height: 60,
                  width: constraints.maxWidth,
                  child: Row(
                    children: [
                      SizedBox(width: 15),
                      Expanded(
                        child: ChooseChildButton(
                          onChildChosen: () {
                            setState(() {
                              isLoading = true;
                            });
                            getGrades(chosenPeriod);
                          },
                        ),
                      ),
                      SizedBox(width: 10),
                      ClipRRect(
                        borderRadius: BorderRadius.circular(5),
                        child: Material(
                          color: Colors.transparent,
                          child: InkWell(
                            child: Container(
                              padding: EdgeInsets.symmetric(vertical: 5, horizontal: 10),
                              child: Row(
                                children: [
                                  Icon(
                                    Icons.date_range,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  SizedBox(width: 8),
                                  Text(
                                    '${chosenPeriod.years}',
                                    style: TextStyle(
                                      color: Colors.white,
                                      fontSize: 18,
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            onTap: () async {
                              var result = await Navigator.push(
                                context,
                                MaterialPageRoute(
                                  builder: (context) => ChooseYearWidget(periods: periods),
                                ),
                              );
                              if (result != null) {
                                if (result != null) {
                                  changePeriod(periods.firstWhere((period) => period.id == result));
                                }
                              }
                            },
                          ),
                        ),
                      ),
                      SizedBox(width: 15),
                    ],
                  ),
                ),
                Container(
                  height: constraints.maxHeight - 60,
                  width: constraints.maxWidth,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(25),
                      topRight: Radius.circular(25),
                    ),
                    color: Theme.of(context).backgroundColor,
                  ),
                  child: Column(
                    children: [
                      Container(
                        height: 74,
                        width: constraints.maxWidth,
                        padding: EdgeInsets.only(top: 15, right: 25, bottom: 10, left: 25),
                        child: FilledInput(
                          onChanged: (value) {
                            setState(() {
                              searchValue = value;
                            });
                          },
                        ),
                      ),
                      Container(
                        height: 1,
                        color: Colors.black38,
                      ),
                      Container(
                        height: constraints.maxHeight - 135,
                        width: constraints.maxWidth,
                        child: SingleChildScrollView(
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.stretch,
                            children: [
                              SizedBox(height: 10),
                              isLoading
                                  ? Container(
                                      child: Column(
                                        children: List.generate(3, (index) => PlaceholderGradesAndAbsencesCard()),
                                      ),
                                    )
                                  : Container(),
                              ...subjects.map(
                                (subject) => Visibility(
                                  visible: subject.title.toLowerCase().indexOf(searchValue.toLowerCase()) >= 0,
                                  child: Card(
                                    margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
                                    elevation: 4,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(10),
                                    ),
                                    child: Container(
                                      padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
                                      child: Column(
                                        children: [
                                          Text(
                                            '${subject.title}',
                                            style: TextStyle(
                                              fontSize: 18,
                                            ),
                                            textAlign: TextAlign.center,
                                          ),
                                          SizedBox(height: 15),
                                          Container(
                                            height: 1,
                                            color: Color(0xFFD1D1D1),
                                          ),
                                          SizedBox(height: 10),
                                          ...subject.subjectGrades.map(
                                            (period) => Container(
                                              margin: EdgeInsets.symmetric(vertical: 10),
                                              child: Row(
                                                crossAxisAlignment: CrossAxisAlignment.start,
                                                children: [
                                                  Text(
                                                    '${period.periodName}',
                                                    style: TextStyle(
                                                      fontSize: 16,
                                                      fontWeight: FontWeight.w500,
                                                    ),
                                                  ),
                                                  SizedBox(width: 20),
                                                  Expanded(
                                                    child: Text(
                                                      '${period.grades != null ? period.grades.join(', ') : ''}',
                                                      softWrap: true,
                                                      style: TextStyle(
                                                        fontSize: 16,
                                                      ),
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ),
                                          ),
                                        ],
                                      ),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(height: 50),
                            ],
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class PlaceholderGradesAndAbsencesCard extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.symmetric(horizontal: 15, vertical: 10),
      elevation: 4,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10),
      ),
      child: Container(
        padding: EdgeInsets.symmetric(horizontal: 20, vertical: 15),
        child: Column(
          children: [
            PlaceholderWidget(width: 150, height: 22),
            SizedBox(height: 15),
            Container(
              height: 1,
              color: Color(0xFFD1D1D1),
            ),
            SizedBox(height: 10),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  SizedBox(width: 20),
                  Expanded(
                    child: PlaceholderWidget(width: 150, height: 20),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  SizedBox(width: 20),
                  Expanded(
                    child: PlaceholderWidget(width: 150, height: 50),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  SizedBox(width: 20),
                  Expanded(
                    child: PlaceholderWidget(width: 70, height: 20),
                  ),
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.symmetric(vertical: 10),
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  PlaceholderWidget(width: 80, height: 20),
                  SizedBox(width: 20),
                  Expanded(
                    child: PlaceholderWidget(width: 150, height: 20),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
